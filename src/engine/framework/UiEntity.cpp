#include "UiEntity.hpp"

UiEntity::UiEntity() : UiEntity(Vector2{0, 0}) {}

UiEntity::UiEntity(const Vector2 &p_position, std::shared_ptr<Sprite> p_sprite, unsigned int p_alignmentFlag)
        : m_position(p_position), m_sprite(p_sprite), m_alignmentFlag(p_alignmentFlag), m_visible(true) {}

UiEntity::UiEntity(float p_x, float p_y, std::shared_ptr<Sprite> p_sprite, unsigned int p_alignmentFlag)
        : UiEntity(Vector2{p_x, p_y}, p_sprite, p_alignmentFlag) {}

const Vector2 &UiEntity::GetPosition() const {
    return m_position;
}

void UiEntity::SetSprite(std::shared_ptr<const Sprite> p_sprite) {
    m_sprite = p_sprite->Clone();
}

const std::shared_ptr<const Sprite> UiEntity::GetSprite() const {
    return m_visible ? m_sprite : nullptr;
}

unsigned int UiEntity::GetAlignmentFlag() const {
    return m_alignmentFlag;
}

void UiEntity::SetVisible(bool p_visible) {
    m_visible = p_visible;
}
