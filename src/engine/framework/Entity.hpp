#pragma once

#include "Animator.hpp"
#include "Sprite.hpp"
#include "Vector2.hpp"

#include <memory>

/**
 * @brief Object that can move in the world and take damage
 * Contain a list of components that are updated each frame
 * 
 */
class Entity {

    public:
        explicit Entity(unsigned int p_health = 10);

        explicit Entity(float p_x, float p_y, unsigned int p_health = 10);

        explicit Entity(const Vector2 &p_position, unsigned int p_health = 10);

        virtual void Update() = 0;

        void UpdateComponents();

        /**
         * @brief Change the callback function when the health change
         * 
         * @param p_healthChangedCallback the callback
         */
        void OnHealthChanged(const std::function<void(unsigned int)> &p_healthChangedCallback);

        void Move(float p_x, float p_y);

        void SetPosition(const Vector2 &p_position);

        void SetPosition(float p_x, float p_y);

        const Vector2 &GetPosition() const;
        
        std::shared_ptr<const Sprite> GetSprite() const;
        
        unsigned int GetHealth() const;

        virtual void TakeDamage(unsigned int p_damageAmount);

    protected:
        template<typename T>
        std::shared_ptr<T> AddComponent(std::shared_ptr<T> p_component) {
            m_components.push_back(p_component);
            return p_component;
        }
        
        virtual void Die();

    protected:
        Vector2 m_position;
        std::shared_ptr<Animator> m_animator;
        std::vector<std::shared_ptr<Component>> m_components;

        /**
         * @brief CallBack function for when the health change (Used to Update UI)
         * 
         */
        std::function<void(unsigned int)> m_healthChangedCallback;

        // Logic gameplay
        unsigned int m_maxHealth;
        unsigned int m_currentHealth;
};


