#pragma once

#include "Sprite.hpp"
#include "Vector2.hpp"

#include <memory>

/**
 * @brief Graphic content displayed on the hud. Can be aligned on the screen easily and rendered visible or not
 * @see Alignment
 */
class UiEntity {

    public:
        explicit UiEntity();

        explicit UiEntity(const Vector2 &p_position, std::shared_ptr<Sprite> p_sprite = nullptr, unsigned int p_alignmentFlag = 0);

        explicit UiEntity(float p_x, float p_y, std::shared_ptr<Sprite> p_sprite = nullptr, unsigned int p_alignmentFlag = 0);

        const Vector2 &GetPosition() const;
        
        void SetSprite(std::shared_ptr<const Sprite> p_sprite);

        const std::shared_ptr<const Sprite> GetSprite() const;

        unsigned int GetAlignmentFlag() const;
        
        void SetVisible(bool p_visible);

    protected:
        Vector2 m_position;
        std::shared_ptr<Sprite> m_sprite;
        unsigned int m_alignmentFlag;
        bool m_visible;
        
};


