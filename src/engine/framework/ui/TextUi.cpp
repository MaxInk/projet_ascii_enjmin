#include "TextUi.hpp"

#include "SpritePool.hpp"

TextUi::TextUi(const Vector2 &p_position, const std::string &p_text, unsigned int p_alignmentFlag) : UiEntity(
        p_position, nullptr, p_alignmentFlag) {
    SetText(p_text);
}

void TextUi::SetText(const std::string &p_text) {
    std::shared_ptr<const Font> baseFont(SpritePool::BASE_FONT);
    std::vector<std::vector<wchar_t>> lines(baseFont->ToAscii(p_text));
    size_t lineLength = lines.empty() ? 0 : lines[0].size();
    std::vector<CHAR_INFO> image(lines.size() * lineLength);
    for (size_t y(0); y < lines.size(); ++y) {
        const std::vector<wchar_t> &line(lines[y]);
        for (size_t x(0); x < lineLength; ++x) {
            CHAR_INFO &charInfo = image[x + y * lineLength];
            charInfo.Char.UnicodeChar = line[x];
            charInfo.Attributes = 7;
        }
    }
    m_sprite = std::make_shared<Sprite>(lineLength, lines.size(), image);
}
