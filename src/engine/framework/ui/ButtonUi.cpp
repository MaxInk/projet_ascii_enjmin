#include "ButtonUi.hpp"

#include "SpritePool.hpp"

ButtonUi::ButtonUi(const Vector2 &p_position, const std::string &p_text, unsigned int p_alignmentFlag) : UiEntity(
        p_position, nullptr, p_alignmentFlag), m_pressedCallback([](){}) {
    SetText(p_text);
}

void ButtonUi::SetText(const std::string &p_text) {
    m_sprite = SpritePool::BUTTON->Clone();
    std::shared_ptr<const Font> baseFont(SpritePool::BASE_FONT);
    std::vector<std::vector<wchar_t>> lines(baseFont->ToAscii(p_text));
    size_t lineLength = lines.empty() ? 0 : lines[0].size();
    size_t xOffset = (m_sprite->GetWidth() - baseFont->GetTextWidth(p_text)) / 2;
    size_t yOffset = (m_sprite->GetHeight() - baseFont->GetFontHeight()) / 2;
    
    for (size_t y(0); y < lines.size(); ++y) {
        const std::vector<wchar_t> &line(lines[y]);
        for (size_t x = 0; x < lineLength; ++x) {
            CHAR_INFO &charInfo = m_sprite->GetChar(x + xOffset, y + yOffset);
            charInfo.Char.UnicodeChar = line[x];
            charInfo.Attributes = 7;
        }
    }
}

void ButtonUi::SetFocused(bool p_focused) {
    WORD attribute(p_focused
            ? FOREGROUND_RED | FOREGROUND_INTENSITY
            : FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    for (size_t y(0); y < m_sprite->GetHeight(); ++y) {
        for (size_t x(0); x < m_sprite->GetWidth(); ++x) {
            m_sprite->GetChar(x, y).Attributes = attribute;
        }
    }
}

void ButtonUi::OnButtonPressed(const std::function<void()> &p_callback) {
    m_pressedCallback = p_callback;
}

void ButtonUi::Press() const {
    m_pressedCallback();
}
