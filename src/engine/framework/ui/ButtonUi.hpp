#pragma once

#include "UiEntity.hpp"

/**
 * @brief Button UI element
 * 
 */
class ButtonUi : public UiEntity {

    public:
        explicit ButtonUi(const Vector2 &p_position, const std::string &p_text, unsigned int p_alignmentFlag = 0);
        
        void SetText(const std::string &p_text);
        
        void SetFocused(bool p_focused);
        
        void Press() const;
        
        void OnButtonPressed(const std::function<void()> &p_callback);

    private:
        std::function<void()> m_pressedCallback;
};


