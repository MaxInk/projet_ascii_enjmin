#pragma once

#include "UiEntity.hpp"

/**
 * @brief Text UI element using Font
 * @see Font
 * 
 */
class TextUi : public UiEntity {
        
    public:
        explicit TextUi(const Vector2 &p_position, const std::string &p_text, unsigned int p_alignmentFlag = 0);

        void SetText(const std::string &p_text);
};


