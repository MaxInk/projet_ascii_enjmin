#include "Entity.hpp"

#include "SpritePool.hpp"

Entity::Entity(unsigned int p_health) : Entity(Vector2{0, 0}, p_health) {}

Entity::Entity(float p_x, float p_y, unsigned int p_health) : Entity(Vector2{p_x, p_y}, p_health) {}

Entity::Entity(const Vector2 &p_position, unsigned int p_health) : m_position(p_position),
                                                                   m_animator(std::make_shared<Animator>()),
                                                                   m_components(),
                                                                   m_healthChangedCallback([](unsigned int) {}),
                                                                   m_maxHealth(p_health),
                                                                   m_currentHealth(m_maxHealth) {
    m_components.push_back(m_animator);
}

void Entity::UpdateComponents() {
    for (const std::shared_ptr<Component> &component : m_components) {
        component->Update();
    }
}

void Entity::OnHealthChanged(const std::function<void(unsigned int)> &p_healthChangedCallback) {
    m_healthChangedCallback = p_healthChangedCallback;
}

void Entity::Move(float p_x, float p_y) {
    m_position.x += p_x;
    m_position.y += p_y;
}

void Entity::SetPosition(const Vector2 &p_position) {
    m_position = p_position;
}

void Entity::SetPosition(float p_x, float p_y) {
    SetPosition({p_x, p_y});
}

const Vector2 &Entity::GetPosition() const {
    return m_position;
}

std::shared_ptr<const Sprite> Entity::GetSprite() const {
    return m_animator->GetCurrentSprite();
}

void Entity::TakeDamage(unsigned int p_damageAmount) {
    if (m_currentHealth == 0) {
        return;
    }
    if (p_damageAmount >= m_currentHealth) {
        m_currentHealth = 0;
        Die();
    } else {
        m_currentHealth -= p_damageAmount;
    }
    m_healthChangedCallback(m_currentHealth);
}

unsigned int Entity::GetHealth() const {
    return m_currentHealth;
}

void Entity::Die() {
    // Do nothing
}
