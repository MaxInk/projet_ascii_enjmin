#pragma once

#include "RenderWindow.hpp"
#include "UiEntity.hpp"

#include <memory>
#include <vector>

/**
 * @brief Represents a part of the game. Can display UiEntity
 * @see UiEntity
 * 
 */
class Scene {
        
    public:
        explicit Scene();

        virtual void Load() = 0;

        virtual void Update() = 0;

        virtual void Render(RenderWindow &p_renderWindow);

    protected:
        void AddUi(std::shared_ptr<UiEntity> p_uiEntity);

    private:
        void RenderUi(RenderWindow &p_renderWindow, std::shared_ptr<UiEntity> &p_uiEntity);

    private:
        std::vector<std::shared_ptr<UiEntity>> m_uiEntities;
};


