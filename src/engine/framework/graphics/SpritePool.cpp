#include "SpritePool.hpp"

#include "AnimationFactory.hpp"
#include "FontFactory.hpp"
#include "SpriteFactory.hpp"

const std::shared_ptr<const Sprite> SpritePool::TODOOM_TITLE(SpriteFactory::LoadSprite("ui/menu/todoom_title"));

const std::shared_ptr<const Sprite> SpritePool::CONTROLS_TITLE(SpriteFactory::LoadSprite("ui/menu/controls_title"));

const std::shared_ptr<const Sprite> SpritePool::CREDITS_TITLE(SpriteFactory::LoadSprite("ui/menu/credits_title"));

const std::shared_ptr<const Sprite> SpritePool::BUTTON(SpriteFactory::LoadSprite("ui/menu/button"));

const std::shared_ptr<const Sprite> SpritePool::KEY(SpriteFactory::LoadSprite("ui/menu/key"));

const std::shared_ptr<const Sprite> SpritePool::BACK_KEY(SpriteFactory::LoadSprite("ui/menu/back_key"));

const std::shared_ptr<const Sprite> SpritePool::ENTER_KEY(SpriteFactory::LoadSprite("ui/menu/enter_key"));

const std::shared_ptr<const Sprite> SpritePool::LIFE_BAR(SpriteFactory::LoadSprite("ui/game/lifebar"));

const std::shared_ptr<const Sprite> SpritePool::CROSSHAIR(SpriteFactory::LoadSprite("ui/game/crosshair"));

const std::shared_ptr<const Sprite> SpritePool::LEVEL_CLEARED(SpriteFactory::LoadSprite("ui/game/level_cleared"));

const std::shared_ptr<const Sprite> SpritePool::YOU_ARE_DEAD(SpriteFactory::LoadSprite("ui/game/you_are_dead"));

const std::shared_ptr<const Sprite> SpritePool::LEVEL_01(SpriteFactory::LoadSprite("ui/game/level01"));

const std::shared_ptr<const Sprite> SpritePool::LEVEL_02(SpriteFactory::LoadSprite("ui/game/level02"));

const std::shared_ptr<const Sprite> SpritePool::LEVEL_03(SpriteFactory::LoadSprite("ui/game/level03"));

const std::shared_ptr<const Animation> SpritePool::MONSTER_IDLE(AnimationFactory::LoadAnimation("monster_idle"));

const std::shared_ptr<const Animation> SpritePool::MONSTER_DEAD(AnimationFactory::LoadAnimation("monster_dead"));

const std::shared_ptr<const Animation> SpritePool::MONSTER_HIT(AnimationFactory::LoadAnimation("monster_hit"));

const std::shared_ptr<const Animation> SpritePool::MONSTER_SHOOT(AnimationFactory::LoadAnimation("monster_shoot"));

const std::shared_ptr<const Animation> SpritePool::PLAYER_IDLE(AnimationFactory::LoadAnimation("player_idle")); 

const std::shared_ptr<const Animation> SpritePool::PLAYER_RUN(AnimationFactory::LoadAnimation("player_run"));

const std::shared_ptr<const Animation> SpritePool::PLAYER_FIRE(AnimationFactory::LoadAnimation("player_fire"));

const std::shared_ptr<const Animation> SpritePool::PLAYER_RELOAD(AnimationFactory::LoadAnimation("player_reload"));

const std::shared_ptr<const Font> SpritePool::BASE_FONT(FontFactory::LoadFont("base"));

