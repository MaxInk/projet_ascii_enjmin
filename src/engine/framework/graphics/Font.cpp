#include "Font.hpp"

#include <sstream>

Font::Font(size_t p_fontHeight, const std::map<char, std::vector<wchar_t>> &p_characters) : m_fontHeight(
        p_fontHeight), m_characters(p_characters) {
}

std::vector<std::vector<wchar_t>> Font::ToAscii(const std::string &p_text) const {
    std::vector<std::vector<wchar_t>> result;
    // For each line of the final ascii
    for (size_t i(0); i < m_fontHeight; ++i) {
        std::vector<wchar_t> line;
        // For all the char of the text
        for (size_t j(0); j < p_text.size(); ++j) {
            std::vector<wchar_t> asciiArray(m_characters.at(p_text[j])); // Get the ascii representation of the char
            size_t charWidth = asciiArray.size() / m_fontHeight; // Get the width of the ascii char
            for (size_t k(0); k < charWidth; ++k) {
                line.push_back(asciiArray[charWidth * i + k]); // Append
            }
        }
        result.push_back(line);
    }
    return result;
}

size_t Font::GetTextWidth(const std::string &p_text) const {
    size_t textWidth = 0;
    // For all the char of the text
    for (size_t j(0); j < p_text.size(); ++j) {
        std::vector<wchar_t> asciiArray(m_characters.at(p_text[j])); // Get the ascii representation of the char
        textWidth += asciiArray.size() / m_fontHeight; // Get the width of the ascii char
    }
    return textWidth;
}

size_t Font::GetFontHeight() const {
    return m_fontHeight;
}
