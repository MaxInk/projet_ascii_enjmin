#pragma once

#include "Sprite.hpp"

#include <memory>
#include <vector>

/**
 * @brief Sequence of sprites with time duration foreach sprite 
 * Generated by AnimationFactory
 * @see AnimationFactory
 * 
 */
class Animation {

    public:
        struct Frame {
            std::shared_ptr<const Sprite> m_sprite;
            float m_displayTime;
        };

        explicit Animation(const std::vector<Frame> &p_frames);

        size_t GetFrameCount() const;

        const Frame &GetFrame(size_t p_index) const;
        
        float GetDuration() const;

    private:
        std::vector<Frame> m_frames;
        float m_duration;
};
