#pragma once
#include "AssetExplorer.hpp"
#include "Sprite.hpp"

#include <fstream>
#include <iostream>
#include <memory>

/**
 * @brief Generates Sprite from .sprite file
 * @see Sprite
 * 
 */
class SpriteFactory {

    public:
        explicit SpriteFactory() = delete;

        explicit SpriteFactory(const SpriteFactory &p_spriteFactory) = delete;

        SpriteFactory &operator=(const SpriteFactory &p_spriteFactory) = delete;

        ~SpriteFactory() = delete;

        static std::shared_ptr<const Sprite> LoadSprite(const std::string &p_spriteName) {
            std::string filePath(AssetExplorer::GetSpritePath(p_spriteName));
            std::wifstream file;
            file.open(filePath, std::fstream::in);
            if (!file.is_open()) {
                std::cerr << "File not found : " << filePath << "." << std::endl;
                return nullptr;
            }

            size_t lineCount;
            size_t columnCount;
            file >> lineCount;
            file >> columnCount;
            std::vector<CHAR_INFO> image(lineCount * columnCount);
            wchar_t ascii;
            file.get();
            for (size_t y(0); y < lineCount; ++y) {
                for (size_t x(0); x < columnCount; ++x) {
                    ascii = file.get();
                    if (ascii != file.eof()) {
                        image[x + y * columnCount].Char.UnicodeChar = ascii;
                    } else {
                        std::cerr << "Error in image parsing (ascii)." << std::endl;
                        return nullptr;
                    }
                }
                file.get();
            }
            int flag;
            for (size_t y(0); y < lineCount; ++y) {
                for (size_t x(0); x < columnCount; ++x) {
                    file >> flag;
                    if (flag != EOF) {
                        image[x + y * columnCount].Attributes = flag;
                    } else {
                        std::cerr << "Error in image parsing (attributes)." << std::endl;
                        return nullptr;
                    }
                }
                file.get();
            }
            file.close();
            return std::make_shared<const Sprite>(columnCount, lineCount, image);
        }
};


