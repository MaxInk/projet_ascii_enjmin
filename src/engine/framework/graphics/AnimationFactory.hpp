#pragma once

#include "Application.hpp"
#include "AssetExplorer.hpp"
#include "SpriteFactory.hpp"

#include <fstream>

/**
 * @brief Generates Animation Object from .animation files
 * @see Animation
 * 
 */
class AnimationFactory {

    public:
        explicit AnimationFactory() = delete;

        explicit AnimationFactory(const AnimationFactory &p_animationFactory) = delete;

        AnimationFactory &operator=(const AnimationFactory &p_animationFactory) = delete;

        ~AnimationFactory() = delete;

        static std::shared_ptr<const Animation> LoadAnimation(const std::string &p_animationName) {
            std::string filePath(AssetExplorer::GetAnimationPath(p_animationName));
            std::ifstream file;
            file.open(filePath, std::fstream::in);
            if (!file.is_open()) {
                std::cerr << "File not found : " << filePath << "." << std::endl;
                return nullptr;
            }

            size_t frameCount;
            file >> frameCount;
            if (frameCount <= 0) {
                std::cerr << "Frame count of : " << filePath << " is invalid." << std::endl;
                return nullptr;
            }
            std::vector<Animation::Frame> frames;
            frames.reserve(frameCount);
            for (size_t i = 0; i < frameCount; ++i) {
                std::string spriteName;
                float displayTime;
                file >> spriteName;
                file >> displayTime;
                frames.push_back(Animation::Frame{SpriteFactory::LoadSprite(spriteName), displayTime / 1000.0f});
            }
            file.close();
            return std::make_shared<const Animation>(frames);
        }

};


