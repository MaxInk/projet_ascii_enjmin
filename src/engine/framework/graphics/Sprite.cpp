#include "Sprite.hpp"

#include <iostream>

Sprite::Sprite(size_t p_width, size_t p_height, const std::vector<CHAR_INFO> &p_image) : m_width(p_width), m_height(p_height),
                                                                      m_image(p_image) {}


Sprite::Sprite(const Sprite &p_sprite) : m_width(p_sprite.m_width), m_height(p_sprite.m_height), m_image() {
    for (size_t i(0); i < p_sprite.m_image.size(); ++i) {
        m_image.push_back(p_sprite.m_image[i]);
    }
}

const CHAR_INFO &Sprite::GetChar(size_t p_x, size_t p_y) const {
    return m_image[p_x + p_y * m_width];
}

CHAR_INFO &Sprite::GetChar(size_t p_x, size_t p_y) {
    return m_image[p_x + p_y * m_width];
}

size_t Sprite::GetWidth() const {
    return m_width;
}

size_t Sprite::GetHeight() const {
    return m_height;
}

std::shared_ptr<Sprite> Sprite::Clone() const {
    return std::make_shared<Sprite>(*this);
}
