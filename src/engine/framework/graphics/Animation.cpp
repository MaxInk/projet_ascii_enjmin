#include "Animation.hpp"

Animation::Animation(const std::vector<Frame> &p_frames) : m_frames(p_frames), m_duration(0.0f) {
    for (const Frame &frame : p_frames) {
        m_duration += frame.m_displayTime;
    }
}

const Animation::Frame &Animation::GetFrame(size_t p_index) const {
    return m_frames[p_index];
}

size_t Animation::GetFrameCount() const {
    return m_frames.size();
}

float Animation::GetDuration() const {
    return m_duration;
}
