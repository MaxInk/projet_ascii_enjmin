#pragma once

#include "UnicodeChar.hpp"

/**
 * @brief Generates Font from a .font file
 * @see Font
 * 
 */
class FontFactory {

    public:
        explicit FontFactory() = delete;

        explicit FontFactory(const FontFactory &p_fontFactory) = delete;

        FontFactory &operator=(const FontFactory &p_fontFactory) = delete;

        ~FontFactory() = delete;

        static std::shared_ptr<const Font> LoadFont(const std::string &p_fontName) {
            std::string filePath(AssetExplorer::GetFontPath(p_fontName));
            std::ifstream file;
            file.open(filePath, std::fstream::in);
            if (!file.is_open()) {
                std::cerr << "File not found : " << filePath << "." << std::endl;
                return nullptr;
            }

            size_t fontHeight;
            file >> fontHeight;
            std::map<char, std::vector<wchar_t>> characters;
            wchar_t c;
            c = file.get(); // \n
            while (!file.eof()) {
                char character(file.get());
                std::vector<wchar_t> asciiChar;
                file.get(); // \n
                // For each line of the char
                for (size_t line(0); line < fontHeight; ++line) {
                    c = file.get();
                    while (c != '\n' && !file.eof()) {
                        asciiChar.push_back(c == '@' ? EMPTY_CHAR : c);
                        c = file.get();
                    }
                }
                characters[character] = asciiChar;
            }
            return std::make_shared<const Font>(fontHeight, characters);
        }
        
};