#pragma once

/**
 * @brief Position alignment for UI elements
 * 
 */
enum Alignment {
    ALIGN_LEFT = 1 << 1,
    ALIGN_RIGHT = 1 << 2,
    ALIGN_TOP = 1 << 3,
    ALIGN_BOTTOM = 1 << 4,
    ALIGN_CENTER = 1 << 5,
};
