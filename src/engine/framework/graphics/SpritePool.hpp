#pragma once

#include "Animation.hpp"
#include "Font.hpp"

#include <memory>

/**
 * @brief Set of all sprites, animations and fonts of the game (UI + Entities)
 * 
 */
class SpritePool {
        
    public:
        explicit SpritePool() = delete;

        explicit SpritePool(const SpritePool &p_spritePool) = delete;

        SpritePool &operator=(const SpritePool &p_spritePool) = delete;
        
        ~SpritePool() = delete;

        static const std::shared_ptr<const Sprite> TODOOM_TITLE;
        static const std::shared_ptr<const Sprite> CONTROLS_TITLE;
        static const std::shared_ptr<const Sprite> CREDITS_TITLE;
        static const std::shared_ptr<const Sprite> BUTTON;
        static const std::shared_ptr<const Sprite> KEY;
        static const std::shared_ptr<const Sprite> BACK_KEY;
        static const std::shared_ptr<const Sprite> ENTER_KEY;
        static const std::shared_ptr<const Sprite> LIFE_BAR;
        static const std::shared_ptr<const Sprite> CROSSHAIR;
        static const std::shared_ptr<const Sprite> LEVEL_CLEARED;
        static const std::shared_ptr<const Sprite> YOU_ARE_DEAD;
        static const std::shared_ptr<const Sprite> LEVEL_01;
        static const std::shared_ptr<const Sprite> LEVEL_02;
        static const std::shared_ptr<const Sprite> LEVEL_03;

        static const std::shared_ptr<const Animation> MONSTER_IDLE;
        static const std::shared_ptr<const Animation> MONSTER_DEAD;
        static const std::shared_ptr<const Animation> MONSTER_HIT;
        static const std::shared_ptr<const Animation> MONSTER_SHOOT;

        static const std::shared_ptr<const Animation> PLAYER_IDLE;
        static const std::shared_ptr<const Animation> PLAYER_RUN;
        static const std::shared_ptr<const Animation> PLAYER_FIRE;
        static const std::shared_ptr<const Animation> PLAYER_RELOAD;
        
        static const std::shared_ptr<const Font> BASE_FONT;
};