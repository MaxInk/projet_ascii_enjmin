#pragma once

#include <map>
#include <vector>

/**
 * @brief ASCI Font to Render UI Text
 * Generated from FontFactory
 * @see FontFactory
 * 
 */
class Font {
        
    public:
        explicit Font(size_t p_fontHeight, const std::map<char, std::vector<wchar_t>> &p_characters);
        
        std::vector<std::vector<wchar_t>> ToAscii(const std::string &p_text) const;
        
        size_t GetTextWidth(const std::string &p_text) const;
        
        size_t GetFontHeight() const;

    private:
        size_t m_fontHeight;
        std::map<char, std::vector<wchar_t>> m_characters;
};


