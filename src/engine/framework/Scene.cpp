#include "Scene.hpp"

#include "Alignment.hpp"

Scene::Scene() : m_uiEntities() {}

void Scene::AddUi(std::shared_ptr<UiEntity> p_uiEntity) {
    m_uiEntities.push_back(p_uiEntity);
}

void Scene::Render(RenderWindow &p_renderWindow) {
    for (std::shared_ptr<UiEntity> &uiEntity: m_uiEntities) {
        RenderUi(p_renderWindow, uiEntity);
    }
}

void Scene::RenderUi(RenderWindow &p_renderWindow, std::shared_ptr<UiEntity> &p_uiEntity) {
    unsigned int alignmentFlag(p_uiEntity->GetAlignmentFlag());
    const std::shared_ptr<const Sprite> sprite(p_uiEntity->GetSprite());
    if (sprite == nullptr) {
        return;
    }

    int xPos(0);
    int yPos(0);

    if ((alignmentFlag & Alignment::ALIGN_CENTER) != 0) {
        xPos = (p_renderWindow.GetRenderWidth() - sprite->GetWidth()) / 2;
        yPos = (p_renderWindow.GetRenderHeight() - sprite->GetHeight()) / 2;
    }
    if ((alignmentFlag & Alignment::ALIGN_TOP) != 0) {
        yPos = 0;
    }
    if ((alignmentFlag & Alignment::ALIGN_BOTTOM) != 0) {
        yPos = p_renderWindow.GetRenderHeight() - sprite->GetHeight() - 1;
    }
    if ((alignmentFlag & Alignment::ALIGN_LEFT) != 0) {
        xPos = 0;
    }
    if ((alignmentFlag & Alignment::ALIGN_RIGHT) != 0) {
        xPos = p_renderWindow.GetRenderWidth() - sprite->GetWidth() - 1;
    }

    xPos += p_uiEntity->GetPosition().x;
    yPos += p_uiEntity->GetPosition().y;

    p_renderWindow.DrawSprite(sprite, xPos, yPos, 1);
}
