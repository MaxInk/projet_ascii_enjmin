#include "Timer.hpp"

Timer::Timer() {
    QueryPerformanceCounter(&m_lastUpdateTime);
    LARGE_INTEGER liFreq;
    QueryPerformanceFrequency(&liFreq);
    m_frequency = liFreq.QuadPart;
    m_frequency /= 1000;
}

void Timer::Start() {
    QueryPerformanceCounter(&m_lastUpdateTime);
}

float Timer::GetElapsedSeconds(bool p_restart) {
    LARGE_INTEGER timeNow;
    QueryPerformanceCounter(&timeNow);
    LONGLONG elapsedLong = timeNow.QuadPart - m_lastUpdateTime.QuadPart;

    float elapsed = (float) ((float) elapsedLong / (float) m_frequency);
    elapsed /= 1000.0f;

    if (p_restart) {
        m_lastUpdateTime = timeNow;
    }
    return elapsed;
}

unsigned long Timer::GetElapsedMs(bool p_restart) {
    LARGE_INTEGER timeNow;
    QueryPerformanceCounter(&timeNow);
    LONGLONG elapsedLong = timeNow.QuadPart - m_lastUpdateTime.QuadPart;

    unsigned long elapsed = (unsigned long) ((float) elapsedLong / (float) m_frequency);
    return elapsed;
}