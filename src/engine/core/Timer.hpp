#pragma once

#include <windows.h>

/**
 * @brief Class that handles time, not depending of a Entity like Cooldown
 * @see Cooldown
 * 
 */
class Timer {
        
    public :
        explicit Timer();

        void Start();

        float GetElapsedSeconds(bool p_restart);

        unsigned long GetElapsedMs(bool p_restart = false);

    private:
        LARGE_INTEGER m_lastUpdateTime;
        LONGLONG m_frequency;
};