#include "Application.hpp"

#include "GameScene.hpp"
#include "MenuScene.hpp"
#include "SpritePool.hpp"

const int Application::SCREEN_WIDTH = 200;
const int Application::SCREEN_HEIGHT = 50;

Application Application::APPLICATION_INSTANCE;

Application::Application() : m_running(false), m_renderWindow(SCREEN_WIDTH, SCREEN_HEIGHT), m_timer(), m_deltaTime(),
                             m_currentScene() {}

Application &Application::Get() {
    return APPLICATION_INSTANCE;
}

void Application::Run() {
    if (m_running) {
        return;
    }
    m_running = true;
    m_currentScene = std::make_shared<MenuScene>();
    m_currentScene->Load();

    float startFrame;
    float endFrame;

    while (m_running) {
        if (IsKeyPressed(VK_ESCAPE)) {
            m_running = false;
            break;
        }
        startFrame = m_timer.GetElapsedMs();

        m_renderWindow.Clear();
        m_currentScene->Update();
        m_currentScene->Render(m_renderWindow);
        m_renderWindow.Render();

        // Compute delta time
        endFrame = m_timer.GetElapsedMs();
        m_deltaTime = endFrame - startFrame;
    } 
}

void Application::Stop() {
    m_running = false;
    m_renderWindow.Close();
}

std::shared_ptr<const Scene> Application::GetCurrentScene() const {
    return m_currentScene;
}

std::shared_ptr<const GameScene> Application::GetCurrentGameScene() const {
    return std::dynamic_pointer_cast<const GameScene>(m_currentScene);
}

void Application::SetCurrentScene(std::shared_ptr<Scene> p_scene) {
    m_currentScene = p_scene;
    m_currentScene->Load();
}

bool Application::IsKeyPressed(int p_key) const {
    return (GetAsyncKeyState(p_key) & 0x8000) != 0;
}

float Application::GetDeltaTime() const {
    return m_deltaTime / 1000.0f;
}