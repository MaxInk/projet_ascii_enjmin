#pragma once

#include "Sprite.hpp"

#include <iostream>
#include <memory>
#include <sstream>
#include <vector>
#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif
BOOL WINAPI GetCurrentConsoleFont(HANDLE p_hConsoleOutput, BOOL p_bMaximumWindow, PCONSOLE_FONT_INFO p_lpConsoleCurrentFont);
#ifdef __cplusplus
}
#endif

/**
 * @brief Class that Handle Bufferised Rendering on the terminal and terminal initialization
 * currently using one buffer for rendering, double buffering could be an option if the framerate is too low
 * 
 */
class RenderWindow {
        
    public:
        explicit RenderWindow(short p_screenWidth, short p_screenHeight);

        explicit RenderWindow(const RenderWindow &p_renderWindow) = delete;

        RenderWindow &operator=(const RenderWindow &p_renderWindow) = delete;

        ~RenderWindow();

        void Render();

        void Clear();
        
        void Close();
        
        void PutChar(char p_char, int p_attribute, int p_x, int p_y);

        void PutChar(char p_char, int p_x, int p_y);

        /**
         * @brief Draw a vertical line on the screen based on the center
         * 
         * @param p_char the character used for the line
         * @param p_x x position on the screen
         * @param p_size size of the line
         */
        void DrawVerticalLine(char p_char, int p_x, int p_size);

        /**
         * @brief Method to write any type of value on top left of the screen mainly used to Debug
         * Debug is shown when Render is called
         * @see Render 
         * 
         * @tparam T must have a valid std::string operator to work fine 
         * @param p_value 
         */
        template <class T> void DebugText(T p_value){
            
            std::stringstream ss;
            ss << p_value;
            m_debugString += ss.str();
            m_debugLineCount++;
        }


        void DrawSprite(std::shared_ptr<const Sprite> p_sprite, int p_x, int p_y, int p_lod = 1);

        /**
         * @brief Draw Entity Sprite in the 3D space, take care of walls order
         * 
         * @param p_sprite Entity Sprite
         * @param p_x x position
         * @param p_y y position
         * @param p_lotDistance Look up table of walls distances to the player (Z buffer)
         * @param p_distance distance of the Entity to the player
         * @param p_maxDistance maximum distance to draw Entity
         */
        void DrawEntity(std::shared_ptr<const Sprite> p_sprite, int p_x, int p_y, const std::vector<double> &p_lotDistance,
                        double p_distance, double p_maxDistance);

        short GetRenderWidth() const;

        short GetRenderHeight() const;

    private:
        /**
         * @brief Set the Console Window Style object
         * Change buffer size and terminal style
         * 
         * @param p_index 
         * @param p_newStyle 
         * @return LONG_PTR 
         */
        LONG_PTR SetConsoleWindowStyle(INT p_index, LONG_PTR p_newStyle);

    private:
        COORD m_screenSize;
        COORD m_bufferSize;
        HWND m_hwndConsole;
        HANDLE m_hOutput;
        CHAR_INFO *m_buffer;
        
        int m_debugLineCount;
        std::string m_debugString;

};
