#include "Audio.hpp"

#include <windows.h>
#include <AssetExplorer.hpp>

void Audio::PlaySfx(const std::string &p_sfxName) {
    std::string filePath(AssetExplorer::GetSfxPath(p_sfxName));
    mciSendString(("stop " + filePath).c_str(), NULL, 0, NULL);
    mciSendStringA(("play " + filePath).c_str(), NULL, 0, NULL);
}

void Audio::PlayBgm(const std::string &p_bgmName) {
    std::string filePath(AssetExplorer::GetBgmPath(p_bgmName));
    PlaySound(filePath.c_str(), NULL, SND_FILENAME  | SND_ASYNC | SND_LOOP | SND_NODEFAULT);
}

void Audio::StopBgm(const std::string &p_bgmName) {
    std::string filePath(AssetExplorer::GetBgmPath(p_bgmName));
    PlaySound(NULL, NULL, 0);
}