#pragma once

#include <string>

/**
 * @brief Handle SFX and BGM
 * 
 */
class Audio {

    public:
        explicit Audio() = delete;

        explicit Audio(const Audio &p_audio) = delete;

        Audio &operator=(const Audio &p_audio) = delete;

        ~Audio() = delete;

        /**
         * @brief Play SFX using mciSendString
         * 
         * @param p_sfxName the name of the audio track to be played
         */
        static void PlaySfx(const std::string &p_sfxName);

        /**
         * @brief Play BGM using PlaySound from Win API
         * 
         * @param p_bgmName the name of the audio track to be played
         */
        static void PlayBgm(const std::string &p_bgmName);

        /**
         * @brief Stop BGM using PlaySound from Win API
         * 
         * @param p_bgmName the name of the audio track to stop
         */
        static void StopBgm(const std::string &p_bgmName);
};


