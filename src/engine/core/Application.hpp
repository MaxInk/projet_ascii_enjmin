#pragma once

#include "GameScene.hpp"
#include "RenderWindow.hpp"
#include "Scene.hpp"
#include "Timer.hpp"

#include <memory>
#include <sstream>
#include <vector>

/**
 * @brief Main class that handle program execution and call other classes
 * @see GameScene
 * @see Timer
 * @see RenderWindow
 * 
 */
class Application {
        
    private:
        explicit Application();

    public:
        explicit Application(const Application &p_application) = delete;

        Application &operator=(const Application &p_application) = delete;

        static Application &Get();

        void Run();
        
        void Stop();
        
        std::shared_ptr<const Scene> GetCurrentScene() const;

        std::shared_ptr<const GameScene> GetCurrentGameScene() const;
        
        void SetCurrentScene(std::shared_ptr<Scene> p_scene);
        
        bool IsKeyPressed(int p_key) const;

        float GetDeltaTime() const;

        /**
         * @brief Method to Screen information on the RenderWindow. Used to Debug issue 
         * 
         * @tparam T 
         * @param p_value 
         */
        template <class T> void DebugText(T p_value) {
            m_renderWindow.template DebugText(p_value);
        }

    public:
        static const int SCREEN_WIDTH;
        static const int SCREEN_HEIGHT;
        
    private:        

        static Application APPLICATION_INSTANCE;
        
        bool m_running;

        RenderWindow m_renderWindow;
        Timer m_timer;
        float m_deltaTime;
        
        std::shared_ptr<Scene> m_currentScene;
};