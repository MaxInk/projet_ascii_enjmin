#include "RenderWindow.hpp"

RenderWindow::RenderWindow(short p_screenWidth, short p_screenHeight)
        : m_bufferSize({p_screenWidth, p_screenHeight}), m_hwndConsole(GetConsoleWindow()), 
        m_hOutput(GetStdHandle(STD_OUTPUT_HANDLE)),
        m_buffer(new CHAR_INFO[GetRenderHeight() * GetRenderWidth()]),
        m_debugLineCount(0) {

    SetConsoleWindowStyle(GWL_STYLE, WS_OVERLAPPEDWINDOW);
    Clear();
}

RenderWindow::~RenderWindow() {
    delete[] m_buffer;
}

void RenderWindow::Render() {
    SMALL_RECT rcRegion = {0, 0, (SHORT) (GetRenderWidth() - 1), (SHORT) (GetRenderHeight() - 1)};
    int x= 0,y=0;
    for (char c: m_debugString) {
        PutChar(c, 7, x, y);

        x++;
        if(x >= m_bufferSize.X){
            x = 0;
            y++;
        }
    }
    WriteConsoleOutput(m_hOutput, m_buffer, m_bufferSize, {0, 0}, &rcRegion);
    SetConsoleCursorPosition(m_hOutput, {0,0});
    m_debugString = "";
}

void RenderWindow::Clear() {
    for (int i(0); i < GetRenderHeight() * GetRenderWidth(); i++) {
        m_buffer[i].Char.UnicodeChar = ' ';
        m_buffer[i].Attributes = 0;
    }
    m_debugLineCount = 0;
}

void RenderWindow::Close() {
    PostMessage(m_hwndConsole, WM_CLOSE, 0, 0);
}

void RenderWindow::PutChar(char p_char, int p_attribute, int p_x, int p_y) {
    if (p_x < 0 || p_x >= m_bufferSize.X || p_y < 0 || p_y >= m_bufferSize.Y) {
        return;
    }

    m_buffer[p_x + (p_y * GetRenderWidth())].Char.UnicodeChar = p_char;
    m_buffer[p_x + (p_y * GetRenderWidth())].Attributes = p_attribute;
}

void RenderWindow::PutChar(char p_char, int p_x, int p_y) {
    PutChar(p_char, 0x09, p_x, p_y);
}

void RenderWindow::DrawVerticalLine(char p_char, int p_x, int p_size) {
    if (p_size < 0) {
        return;
    }
    int yStart = GetRenderHeight() / 2 - p_size / 2;

    for (int i(yStart); i < yStart + p_size; i++) {
        PutChar(p_char, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN, p_x, i);
    }
}

void RenderWindow::DrawSprite(std::shared_ptr<const Sprite> p_sprite, int p_x, int p_y, int p_lod) {
    size_t height(p_sprite->GetHeight());
    size_t width(p_sprite->GetWidth());
    for (size_t y(0), yLod(0); y < height; yLod++, y += p_lod) {
        for (size_t x = 0, xLod = 0; x < width; xLod++, x += p_lod) {
            const CHAR_INFO &charInfo(p_sprite->GetChar(x, y));
            if (charInfo.Char.UnicodeChar != ' ') {
                PutChar(charInfo.Char.UnicodeChar, charInfo.Attributes, p_x + xLod, p_y + yLod);
            }
        }
    }
}

void RenderWindow::DrawEntity(std::shared_ptr<const Sprite> p_sprite, int p_x, int p_y, const std::vector<double> &p_lotDistance, double p_distance, double p_maxDistance){
    p_maxDistance /= 2;
    float lod((p_distance * 10.0f) / (p_maxDistance - 1.0f) + 1.0f);
    if (lod >= 10.0f) {
        return;
    }
    p_x -= p_sprite->GetWidth() / 2 / lod; // Half of the size of the sprite
    p_y -= p_sprite->GetHeight() / 2 / lod; // Half of the size of the sprite
    
    for (float y(0.0f), yLod(0); y < p_sprite->GetHeight(); yLod++, y += lod) {
        for (float x(0.0f), xLod(0); x < p_sprite->GetWidth(); xLod++, x += lod) {
            if (m_bufferSize.X - (p_x + xLod) < p_lotDistance.size() && p_lotDistance[m_bufferSize.X - (p_x + xLod)] > p_distance) {
                CHAR_INFO charInfo(p_sprite->GetChar(x, y));
                if (charInfo.Char.AsciiChar != ' ') {
                    PutChar(charInfo.Char.AsciiChar, charInfo.Attributes, p_x + xLod, p_y + yLod);
                }
            }
        }
    }
}

LONG_PTR RenderWindow::SetConsoleWindowStyle(INT p_index, LONG_PTR p_newStyle) {
    SetLastError(NO_ERROR);
    LONG_PTR style = SetWindowLongPtr(m_hwndConsole, p_index, p_newStyle);
    SetWindowLong(m_hwndConsole, GWL_STYLE, GetWindowLong(m_hwndConsole, GWL_STYLE) & ~WS_MAXIMIZEBOX & ~WS_SIZEBOX);

    CONSOLE_FONT_INFO fontInfo;
    GetCurrentConsoleFont(m_hOutput, false, &fontInfo);

    // m_bufferSize.X = (m_screenSize.X / fontInfo.dwFontSize.X-2);
    // m_bufferSize.Y = (m_screenSize.Y / fontInfo.dwFontSize.Y-2);  // Visibly H need a shift
    m_screenSize.X = GetRenderWidth() * fontInfo.dwFontSize.X;
    m_screenSize.Y = GetRenderHeight() * (fontInfo.dwFontSize.Y + 1) - 8;
    SetConsoleScreenBufferSize(m_hOutput, m_bufferSize);

    // RECT windowsRect;
    // ClipCursor( &windowsRect );

    SetWindowPos(m_hwndConsole, 0, 0, 0, m_screenSize.X, m_screenSize.Y,
                 SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE | SWP_DRAWFRAME);
    SetConsoleTitleA("TODOOM");
    ShowScrollBar(m_hwndConsole, SB_BOTH, false);
    ShowWindow(m_hwndConsole, SW_SHOW);
    return style;
}

short RenderWindow::GetRenderWidth() const {
    return m_bufferSize.X;
}

short RenderWindow::GetRenderHeight() const {
    return m_bufferSize.Y;
}
