#include "Animator.hpp"

#include "Application.hpp"

Animator::Animator() : Component(), m_states(), m_nextFrameTriggers(), m_booleans(), m_currentState(nullptr),
                       m_currentFrameIndex(-1.0f), m_currentFrameTimeElapsed(0.0f) {}

void Animator::Update() {
    float deltaTime(Application::Get().GetDeltaTime());
    m_currentFrameTimeElapsed += deltaTime;

    for (const Transition &transition : m_currentState.m_transitions) {
        if (!transition.m_finishBeforeTransition && !transition.m_conditions.empty() && AreConditionsTrue(transition)) {
            SetState(transition.m_stateDestination);
            return;
        }
    }

    std::shared_ptr<const Animation> &currentAnimation(m_currentState.m_animation);
    Animation::Frame currentFrame(currentAnimation->GetFrame(m_currentFrameIndex));
    while (currentFrame.m_displayTime > 0.0f && m_currentFrameTimeElapsed > currentFrame.m_displayTime) {
        m_currentFrameTimeElapsed -= currentFrame.m_displayTime;
        if (m_currentFrameTimeElapsed < 0.0f) {
            m_currentFrameTimeElapsed = 0.0f;
        }
        // Play next frame
        if (m_currentFrameIndex < currentAnimation->GetFrameCount() - 1) {
            m_currentFrameIndex++;
            currentFrame = currentAnimation->GetFrame(m_currentFrameIndex);
        } else if (m_currentState.m_loop) {
            m_currentFrameIndex = 0;
            currentFrame = currentAnimation->GetFrame(m_currentFrameIndex);
        } else {
            // Animation finished
            // Let's find a simple transition
            for (const Transition &transition : m_currentState.m_transitions) {
                if (transition.m_conditions.empty()) {
                    SetState(transition.m_stateDestination);
                    return;
                }
            }
        }
    }

    m_nextFrameTriggers.clear();
}

AnimationState &Animator::AddState(std::shared_ptr<const Animation> p_animation) {
    m_states.emplace_back(p_animation);
    m_states.back().m_stateIndex = m_states.size() - 1;
    return m_states.back();
    
}

Transition &Animator::AddTransition(size_t p_animationKeySource, size_t p_animationKeyDestination) {
    std::vector<Transition> &transitions(m_states[p_animationKeySource].m_transitions);
    transitions.emplace_back(p_animationKeyDestination);
    return transitions.back();
}

void Animator::SetState(size_t p_stateIndex) {
    m_currentState = m_states[p_stateIndex];
    m_currentFrameIndex = 0;
    m_currentFrameTimeElapsed = 0.0f;
}

void Animator::SetTrigger(const std::string &p_trigger) {
    m_nextFrameTriggers.insert(p_trigger);
}

void Animator::SetBool(const std::string &p_name, bool p_value) {
    if (p_value) {
        m_booleans.insert(p_name);
    } else {
        m_booleans.erase(p_name);
    }
}

std::shared_ptr<const Sprite> Animator::GetCurrentSprite() const {
    return m_currentState.m_animation
           ? m_currentState.m_animation->GetFrame(m_currentFrameIndex).m_sprite
           : nullptr;
}

bool Animator::AreConditionsTrue(const Transition &p_transition) const {
    for (const Transition::Condition &condition : p_transition.m_conditions) {
        if (condition.m_type == Transition::Condition::ConditionType::TRIGGER) {
            if (!ContainsTrigger(condition.m_name)) {
                return false;
            }
        } else if (condition.m_type == Transition::Condition::ConditionType::BOOL) {
            if (!GetBool(condition.m_name)) {
                return false;
            } 
        } else if (condition.m_type == Transition::Condition::ConditionType::NOT_BOOL) {
            if (GetBool(condition.m_name)) {
                return false;
            }
        }
    }
    return true;
}

bool Animator::ContainsTrigger(const std::string &p_trigger) const {
    return m_nextFrameTriggers.find(p_trigger) != m_nextFrameTriggers.end();
}

bool Animator::GetBool(const std::string &p_bool) const {
    return m_booleans.find(p_bool) != m_booleans.end();
}
