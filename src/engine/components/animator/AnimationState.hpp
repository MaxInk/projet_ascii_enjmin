#pragma once

#include "Transition.hpp"

class Animator;

/**
 * @brief State in Animator FSM with animation and his transitions, can loop or play once
 * @see Transition
 * @see Animator
 * 
 */
class AnimationState {
        
    public:
        explicit AnimationState(std::shared_ptr<const Animation> p_animation)
                : m_animation(p_animation), m_transitions(), m_loop(false) {}

        explicit AnimationState(const AnimationState &p_animationState) = default;

        AnimationState& operator=(const AnimationState &p_animationState) = default;

        operator size_t() const {
            return m_stateIndex;
        }

        AnimationState &SetLoop(bool p_loop) {
            m_loop = p_loop;
            return *this;
        }

    private:
        std::shared_ptr<const Animation> m_animation;
        std::vector<Transition> m_transitions;
        bool m_loop;
        size_t m_stateIndex;

        friend Animator;
};


