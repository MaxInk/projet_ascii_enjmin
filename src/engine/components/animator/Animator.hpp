#pragma once

#include "Animation.hpp"
#include "AnimationState.hpp"
#include "Component.hpp"
#include "Sprite.hpp"
#include "Transition.hpp"

#include <map>
#include <memory>
#include <set>

class Entity;

/**
 * @brief Component that handle animation, FSM consist of states and contain logic behind (triggers and boolean)
 * @see AnimationState
 * @see Transition
 * @see Component
 * 
 */
class Animator : public Component {
        
    public:
        explicit Animator();

        explicit Animator(const Animator &p_animator) = delete;

        Animator& operator=(const Animator &p_animator) = delete;

        void Update() override;

        AnimationState& AddState(std::shared_ptr<const Animation> p_animation);

        Transition &AddTransition(size_t p_animationKeySource, size_t p_animationKeyDestination);

        void SetState(size_t p_stateIndex);

        void SetTrigger(const std::string &p_trigger);
        
        void SetBool(const std::string &p_name, bool p_value);

        std::shared_ptr<const Sprite> GetCurrentSprite() const;

    private:
        bool AreConditionsTrue(const Transition &p_transition) const;
        
        bool ContainsTrigger(const std::string &p_trigger) const;
        
        bool GetBool(const std::string &p_bool) const;

    private:
        std::vector<AnimationState> m_states;
        std::set<std::string> m_nextFrameTriggers;
        std::set<std::string> m_booleans;

        AnimationState m_currentState;
        float m_currentFrameIndex;
        float m_currentFrameTimeElapsed;
        
        friend Transition;
};


