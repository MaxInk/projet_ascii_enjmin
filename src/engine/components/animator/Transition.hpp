#pragma once

class Animator;

/**
 * @brief Transition between AnimationState if condition is fulfilled
 * @see Animator
 * @see AnimationState
 */
class Transition {
        
    public:
        /**
         * @brief Condition can be 
         * TRIGGER (string) check if the trigger is in the Animator
         * BOOL check if true or false
         * NOT_BOOL inverted BOOL
         * 
         */
        class Condition {
            public:
                enum class ConditionType {
                    TRIGGER, BOOL, NOT_BOOL
                };
                explicit Condition(const std::string &p_name, ConditionType p_type) : m_name(p_name), m_type(p_type) {}

            private:
                std::string m_name;
                ConditionType m_type;
                
                friend Animator;
        };
        
        explicit Transition(size_t p_stateDestination) : m_stateDestination(p_stateDestination), m_conditions(),
                                                m_finishBeforeTransition(true) {}

        explicit Transition(const Transition &p_transition) = default;

        Transition& operator=(const Transition &p_transition) = default;

        /**
         * @brief When a trigger is detected
         * 
         * @param p_trigger trigger identification
         * @return Transition& transition with new trigger condition
         */
        Transition &WhenTrigger(const std::string &p_trigger) {
            m_conditions.emplace_back(p_trigger, Condition::ConditionType::TRIGGER);
            return *this;
        }

        /**
         * @brief When a bool is detected
         * 
         * @param p_bool bool identification
         * @param p_value 
         * @return Transition& transition with new bool condition
         */
        Transition &WhenBool(const std::string &p_bool, bool p_value) {
            if (p_value) {
                m_conditions.emplace_back(p_bool, Condition::ConditionType::BOOL);
            } else{
                m_conditions.emplace_back(p_bool, Condition::ConditionType::NOT_BOOL);
            }
            return *this;
        }
        
        /**
         * @brief Does the transition need to wait the Animation to finish
         * 
         * @param p_finishBeforeTransition 
         * @return Transition& 
         */
        Transition &FinishBeforeTransition(bool p_finishBeforeTransition) {
            m_finishBeforeTransition = p_finishBeforeTransition;
            return *this;
        }

    private:
        size_t m_stateDestination;
        std::vector<Condition> m_conditions;
        bool m_finishBeforeTransition;

        friend Animator;
};
