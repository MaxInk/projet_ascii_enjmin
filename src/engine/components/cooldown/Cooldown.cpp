#include "Cooldown.hpp"

#include "Application.hpp"

Cooldown::Cooldown(float p_duration) : m_duration(p_duration), m_elapsing(false), m_timeElapsed(0.0f) {}

void Cooldown::Update() {
    float deltaTime(Application::Get().GetDeltaTime());
    if (!m_elapsing) {
        return;
    }
    m_timeElapsed += deltaTime;
    if (m_timeElapsed > m_duration) {
        m_elapsing = false;
    }
}

void Cooldown::Elapse() {
    m_timeElapsed = 0.0f;
    m_elapsing = true;
}

bool Cooldown::IsElapsing() const {
    return m_elapsing;
}

void Cooldown::Stop() {
    m_elapsing = false;
}
