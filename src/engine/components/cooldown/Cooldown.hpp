#pragma once

#include "Component.hpp"

/**
 * @brief Component that handle time
 * @see Component
 * 
 */
class Cooldown : public Component {

    public:
        explicit Cooldown(float p_duration);
        
        void Update() override;
        
        void Elapse();
        
        bool IsElapsing() const;
        
        void Stop();
        
    private:
        float m_duration;
        bool m_elapsing;
        float m_timeElapsed;
};


