#pragma once

/**
 * @brief Abstract object linked to Entities
 * 
 */
class Component {
        
    public:
        virtual void Update() = 0;
};


