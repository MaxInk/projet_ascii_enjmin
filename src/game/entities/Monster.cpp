#include "Monster.hpp"

#include "Audio.hpp"

Monster::Monster(float p_x, float p_y, std::shared_ptr<Entity> p_target) : Entity(Vector2{p_x, p_y}, 50),
                                                                           m_target(p_target), m_hitDistance(1),
                                                                           m_hitDamage(10),
                                                                           m_hitCooldown(AddComponent<Cooldown>(
                                                                                   std::make_shared<Cooldown>(1.5f))) {
    size_t idleState = m_animator->AddState(SpritePool::MONSTER_IDLE).SetLoop(true);
    size_t hitState = m_animator->AddState(SpritePool::MONSTER_HIT).SetLoop(false);
    size_t deadState = m_animator->AddState(SpritePool::MONSTER_DEAD).SetLoop(true);
    size_t shootState = m_animator->AddState(SpritePool::MONSTER_SHOOT).SetLoop(false);
    
    m_animator->AddTransition(idleState, hitState).WhenTrigger("hit").FinishBeforeTransition(false);
    m_animator->AddTransition(idleState, deadState).WhenBool("dead", true).FinishBeforeTransition(false);
    m_animator->AddTransition(idleState, shootState).WhenTrigger("shoot").FinishBeforeTransition(false);
    
    m_animator->AddTransition(hitState, idleState).FinishBeforeTransition(true);
    m_animator->AddTransition(shootState, idleState).FinishBeforeTransition(true);
    
    m_animator->SetState(idleState);
}

void Monster::Update() {
    if (m_target == nullptr || m_currentHealth == 0) {
        return;
    }
    
    if (m_position.Distance(m_target->GetPosition()) < m_hitDistance && !m_hitCooldown->IsElapsing()) {
        m_animator->SetTrigger("shoot");
        Audio::PlaySfx("monster_attacks");
        m_target->TakeDamage(m_hitDamage);
        m_hitCooldown->Elapse();
    } else {
        const Vector2 &targetPosition(m_target->GetPosition());
        if (targetPosition.Distance(m_position) <= m_hitDistance / 2.0f) {
            return;
        }
        
        float deltaTime(Application::Get().GetDeltaTime());
        float speed(0.5f * deltaTime);
        
        Vector2 direction(Vector2{targetPosition.x - m_position.x, targetPosition.y - m_position.y});
        direction.Normalize();

        int newPositionX(round(m_position.x + direction.x * speed));
        int newPositionY(round(m_position.y + direction.y * speed));
        if (Application::Get().GetCurrentGameScene()->IsItColliding(newPositionX, m_position.y)) {
            direction.x = 0;
        }
        if (Application::Get().GetCurrentGameScene()->IsItColliding(m_position.x, newPositionY)) {
            direction.y = 0;
        }
        Move(direction.x * speed, direction.y * speed);
    }
}

void Monster::TakeDamage(unsigned int p_damageAmount) {
    if (m_currentHealth > 0) {
        m_animator->SetTrigger("hit");
        Audio::PlaySfx("monster_receives_damage");
    }
    Entity::TakeDamage(p_damageAmount);
}

void Monster::SetTarget(std::shared_ptr<Entity> p_target) {
    m_target = p_target;
}

void Monster::Die() {
    m_animator->SetBool("dead", true);
    Audio::PlaySfx("monster_dies");
}
