#include "PlayerIdleState.hpp"

#include "Application.hpp"

PlayerIdleState::PlayerIdleState(Player &p_player) : PlayerMovementState(p_player) {}

void PlayerIdleState::OnEnterState() {
    m_player.m_animator->SetBool("run", false);
}

void PlayerIdleState::OnRotate(int p_input) {
    float deltaTime(Application::Get().GetDeltaTime());
    m_player.Rotate(m_player.ROTATE_SPEED * p_input * deltaTime);
}

void PlayerIdleState::OnMove(int p_input) {
    if (p_input != 0) {
        m_player.SetCurrentMovementState(m_player.m_runState);
    }
}
