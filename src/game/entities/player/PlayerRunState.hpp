#pragma once

#include "PlayerMovementState.hpp"

class PlayerRunState : public PlayerMovementState {

    public:
        explicit PlayerRunState(Player &p_player);

        void OnEnterState() override;

        void OnRotate(int p_input) override;

        void OnMove(int p_input) override;
        
};


