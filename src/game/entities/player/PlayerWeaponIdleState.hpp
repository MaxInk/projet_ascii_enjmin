#pragma once

#include "PlayerWeaponState.hpp"

class PlayerWeaponIdleState : public PlayerWeaponState {
    
    public:
        explicit PlayerWeaponIdleState(Player &p_player);
        
        void OnShootPressed() override;

        void OnReloadPressed() override;
};


