#pragma once

class Player;

class PlayerState {
        
    public:
        explicit PlayerState(Player &p_player) : m_player(p_player) {}

        virtual void OnEnterState() {
            // Do nothing
        }
        
        virtual void Update() {
            // Do nothing
        }

    protected:
        Player &m_player;
};