#pragma once

#include "Entity.hpp"
#include "Cooldown.hpp"
#include "PlayerMovementState.hpp"
#include "PlayerState.hpp"
#include "PlayerWeaponState.hpp"
#include "Vector2.hpp"

class PlayerIdleState;
class PlayerRunState;
class PlayerWeaponIdleState;
class PlayerShootState;
class PlayerReloadState;

/**
 * @brief The player of the application. Can move, turn and shoot.
 * Has different states to manage the different possible behaviors
 * @see PlayerState
 */
class Player : public Entity {

    public:
        explicit Player(float p_x, float p_y, float p_o);
        
        void Update() override;

        void OnAmmoChanged(const std::function<void(unsigned int)> &p_ammoChangedCallback);

        void OnStep(const std::function<void()> &p_stepCallback);

        void OnShooting(const std::function<void()> &p_shotCallback);

        float GetOrientation() const;
        
        unsigned int GetAmmoMaxCount() const;
        
    private:
        void Rotate(float p_angle);

        void SetCurrentMovementState(std::shared_ptr<PlayerMovementState> p_movementState);

        void SetCurrentWeaponState(std::shared_ptr<PlayerWeaponState> p_weaponState);

        static int ProcessRotateInput();

        static int ProcessMoveInput();

        void PlayFootStep();

    private:
        static const float ROTATE_SPEED;
        static const float MOVE_SPEED;

        // States
        std::shared_ptr<PlayerMovementState> m_idleState;
        std::shared_ptr<PlayerMovementState> m_runState;
        
        std::shared_ptr<PlayerWeaponState> m_weaponIdleState;
        std::shared_ptr<PlayerWeaponState> m_shootState;
        std::shared_ptr<PlayerWeaponState> m_reloadState;

        std::shared_ptr<PlayerMovementState> m_currentMovementState;
        std::shared_ptr<PlayerWeaponState> m_currentWeaponState;

        // Logic gameplay
        float m_orientation;
        unsigned int m_ammoMaxCount;
        unsigned int m_ammoCount;
        std::shared_ptr<Cooldown> m_fireCooldown;

        // Foot steps
        std::shared_ptr<Cooldown> m_footStepSoundSpacing;
        
        // Callbacks
        std::function<void(unsigned int)> m_ammoChangedCallback;
        std::function<void()> m_stepCallback;
        std::function<void()> m_shotCallback;

        friend PlayerIdleState;
        friend PlayerRunState;
        friend PlayerWeaponIdleState;
        friend PlayerShootState;
        friend PlayerReloadState;
};


