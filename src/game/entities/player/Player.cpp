#include "Player.hpp"

#include "Application.hpp"
#include "PlayerIdleState.hpp"
#include "PlayerReloadState.hpp"
#include "PlayerRunState.hpp"
#include "PlayerShootState.hpp"
#include "PlayerWeaponIdleState.hpp"
#include "SpritePool.hpp"

const float Player::ROTATE_SPEED(2.0f);
const float Player::MOVE_SPEED(5.0f);

Player::Player(float p_x, float p_y, float p_o) : Entity(Vector2{p_x, p_y}, 100),
                                                  m_idleState(std::make_shared<PlayerIdleState>(*this)),
                                                  m_runState(std::make_shared<PlayerRunState>(*this)),
                                                  m_weaponIdleState(std::make_shared<PlayerWeaponIdleState>(*this)),
                                                  m_shootState(std::make_shared<PlayerShootState>(*this)),
                                                  m_reloadState(std::make_shared<PlayerReloadState>(*this)),
                                                  m_currentMovementState(m_idleState),
                                                  m_currentWeaponState(m_weaponIdleState),
                                                  m_orientation(p_o), //m_maxHealth(100), m_currentHealth(m_maxHealth),
                                                  m_ammoMaxCount(5), m_ammoCount(m_ammoMaxCount), m_fireCooldown(
                                                          AddComponent<Cooldown>(std::make_shared<Cooldown>(0.5f))),
                                                  m_footStepSoundSpacing(
                                                          AddComponent<Cooldown>(std::make_shared<Cooldown>(0.5f))),
                                                  m_ammoChangedCallback([](unsigned int) {}),
                                                  m_stepCallback([]() {}),
                                                  m_shotCallback([]() {}) {
    size_t idleState = m_animator->AddState(SpritePool::PLAYER_IDLE).SetLoop(true);
    size_t runState = m_animator->AddState(SpritePool::PLAYER_RUN).SetLoop(true);
    size_t fireState = m_animator->AddState(SpritePool::PLAYER_FIRE).SetLoop(false);
    size_t reloadState = m_animator->AddState(SpritePool::PLAYER_RELOAD).SetLoop(false);

    m_animator->AddTransition(idleState, runState).WhenBool("run", true).FinishBeforeTransition(false);
    m_animator->AddTransition(idleState, fireState).WhenTrigger("fire").FinishBeforeTransition(false);
    m_animator->AddTransition(idleState, reloadState).WhenTrigger("reload").FinishBeforeTransition(false);

    m_animator->AddTransition(fireState, reloadState).WhenTrigger("reload").FinishBeforeTransition(false);
    m_animator->AddTransition(fireState, idleState).FinishBeforeTransition(true);

    m_animator->AddTransition(reloadState, idleState).FinishBeforeTransition(true);

    m_animator->AddTransition(runState, idleState).WhenBool("run", false).FinishBeforeTransition(false);
    m_animator->AddTransition(runState, fireState).WhenTrigger("fire").FinishBeforeTransition(false);
    m_animator->AddTransition(runState, reloadState).WhenTrigger("reload").FinishBeforeTransition(false);

    m_animator->SetState(idleState);
}

void Player::Update() {
    m_currentMovementState->OnRotate(ProcessRotateInput());
    m_currentMovementState->OnMove(ProcessMoveInput());

    if (Application::Get().IsKeyPressed(VK_RETURN)) {
        m_currentWeaponState->OnShootPressed();
    }
    
    if (Application::Get().IsKeyPressed('R')) {
        m_currentWeaponState->OnReloadPressed();
    }

    m_currentMovementState->Update();
    m_currentWeaponState->Update();
}

void Player::OnAmmoChanged(const std::function<void(unsigned int)> &p_ammoChangedCallback) {
    m_ammoChangedCallback = p_ammoChangedCallback;
}

void Player::OnStep(const std::function<void()> &p_stepCallback) {
    m_stepCallback = p_stepCallback;
}

void Player::OnShooting(const std::function<void()> &p_shotCallback) {    
    m_shotCallback = p_shotCallback;
}

void Player::Rotate(float p_angle) {
    m_orientation += p_angle;
}

float Player::GetOrientation() const {
    return m_orientation;
}

unsigned int Player::GetAmmoMaxCount() const {
    return m_ammoMaxCount;
}

void Player::SetCurrentMovementState(std::shared_ptr<PlayerMovementState> p_movementState) {
    m_currentMovementState = p_movementState;
    if (m_currentMovementState) {
        m_currentMovementState->OnEnterState();
    }
}

void Player::SetCurrentWeaponState(std::shared_ptr<PlayerWeaponState> p_weaponState) {
    m_currentWeaponState = p_weaponState;
    if (m_currentWeaponState) {
        m_currentWeaponState->OnEnterState();
    }
}

int Player::ProcessRotateInput() {
    int rotateInput(0);
    if (Application::Get().IsKeyPressed('Q')) {
        rotateInput++;
    }
    if (Application::Get().IsKeyPressed('D')) {
        rotateInput--;
    }
    return rotateInput;
}

int Player::ProcessMoveInput() {
    int moveInput(0);
    if (Application::Get().IsKeyPressed('Z')) {
        moveInput++;
    }
    if (Application::Get().IsKeyPressed('S')) {
        moveInput--;
    }
    return moveInput;
}

void Player::PlayFootStep() {
    if (!m_footStepSoundSpacing->IsElapsing()){
        m_stepCallback();
        m_footStepSoundSpacing->Elapse();
    }
}
