#pragma once

#include "PlayerMovementState.hpp"

class PlayerIdleState : public PlayerMovementState {

    public:
        explicit PlayerIdleState(Player &p_player);

        void OnEnterState() override;

        void OnRotate(int p_input) override;

        void OnMove(int p_input) override;
};


