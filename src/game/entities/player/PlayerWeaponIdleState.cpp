#include "PlayerWeaponIdleState.hpp"

#include "Player.hpp"

PlayerWeaponIdleState::PlayerWeaponIdleState(Player &p_player) : PlayerWeaponState(p_player) {}

void PlayerWeaponIdleState::OnShootPressed() {
    if (m_player.m_ammoCount > 0 && !m_player.m_fireCooldown->IsElapsing()) {
        m_player.SetCurrentWeaponState(m_player.m_shootState);
    }
}

void PlayerWeaponIdleState::OnReloadPressed() {
    if (m_player.m_ammoCount != m_player.m_ammoMaxCount) {
        m_player.SetCurrentWeaponState(m_player.m_reloadState);
    }
}
