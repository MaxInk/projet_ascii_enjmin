#pragma once

#include "PlayerState.hpp"

class PlayerMovementState : public PlayerState {
        
    public:
        explicit PlayerMovementState(Player &p_player) : PlayerState(p_player) {}

        virtual void OnRotate(int p_input) {
            // Do nothing
        }

        virtual void OnMove(int p_input) {
            // Do nothing
        }
};