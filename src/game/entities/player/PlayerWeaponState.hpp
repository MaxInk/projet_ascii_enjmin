#pragma once

#include "PlayerState.hpp"

class PlayerWeaponState : public PlayerState {
    
    public:
        explicit PlayerWeaponState(Player &p_player) : PlayerState(p_player) {}
        
        virtual void OnShootPressed() {
            // Do nothing
        }
        
        virtual void OnReloadPressed() {
            // Do nothing
        }
};


