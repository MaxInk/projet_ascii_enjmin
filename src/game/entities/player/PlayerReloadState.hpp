#pragma once

#include "Cooldown.hpp"
#include "PlayerWeaponState.hpp"

class PlayerReloadState : public PlayerWeaponState {
    
    public:
        explicit PlayerReloadState(Player &p_player);

        void OnEnterState() override;
        
        void Update() override;
        
    private:
        Cooldown m_reloadDuration;
};


