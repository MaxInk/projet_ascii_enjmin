#pragma once

#include "Cooldown.hpp"
#include "PlayerWeaponState.hpp"

class PlayerShootState : public PlayerWeaponState {
    
    public:
        explicit PlayerShootState(Player &p_player);

        void OnEnterState() override;
        
        void Update() override;
        
    private:
        Cooldown m_shootDuration;
};


