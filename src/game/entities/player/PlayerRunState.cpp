#include "PlayerRunState.hpp"

#include "Application.hpp"

PlayerRunState::PlayerRunState(Player &p_player) : PlayerMovementState(p_player) {}

void PlayerRunState::OnEnterState() {
    m_player.m_animator->SetBool("run", true);
}

void PlayerRunState::OnRotate(int p_input) {
    float deltaTime(Application::Get().GetDeltaTime());
    m_player.Rotate(m_player.ROTATE_SPEED * p_input * deltaTime);
}

void PlayerRunState::OnMove(int p_input) {
    if (p_input == 0) {
        m_player.SetCurrentMovementState(m_player.m_idleState);
        return;
    }
    
    float deltaTime(Application::Get().GetDeltaTime());
    float orientation(m_player.m_orientation);
    Vector2 &position(m_player.m_position);
    
    float moveAmount(m_player.MOVE_SPEED * p_input * deltaTime);
    Vector2 moveForce({sinf(orientation) * moveAmount, cosf(orientation) * moveAmount});
    if (Application::Get().GetCurrentGameScene()->IsItColliding(position.x + moveForce.x, position.y)) {
        moveForce.x = 0;
    }
    if (Application::Get().GetCurrentGameScene()->IsItColliding(position.x, position.y + moveForce.y)) {
        moveForce.y = 0;
    }
    m_player.Move(moveForce.x, moveForce.y);
    m_player.PlayFootStep();
}
