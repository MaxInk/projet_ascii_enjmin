#include "PlayerShootState.hpp"

#include "Application.hpp"
#include "SpritePool.hpp"

PlayerShootState::PlayerShootState(Player &p_player) : PlayerWeaponState(p_player),
                                                       m_shootDuration(SpritePool::PLAYER_FIRE->GetDuration()) {}

void PlayerShootState::OnEnterState() {
    m_player.m_ammoCount--;
    m_player.m_animator->SetTrigger("fire");
    m_player.m_fireCooldown->Elapse();
    m_shootDuration.Elapse();
    m_player.m_shotCallback();
    
    std::shared_ptr<const GameScene> gameScene = Application::Get().GetCurrentGameScene();
    std::shared_ptr<Entity> pointingEntity = gameScene->GetPointingEntity();
    if (pointingEntity) {
        pointingEntity->TakeDamage(20);
    }
}

void PlayerShootState::Update() {
    m_shootDuration.Update();
    if (!m_shootDuration.IsElapsing()) {
        m_player.m_ammoChangedCallback(m_player.m_ammoCount);
        if (m_player.m_ammoCount == 0) {
            m_player.SetCurrentWeaponState(m_player.m_reloadState);
        } else {
            m_player.SetCurrentWeaponState(m_player.m_weaponIdleState);
        }
    }
}

