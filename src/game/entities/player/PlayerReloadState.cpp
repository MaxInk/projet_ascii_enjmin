#include "PlayerReloadState.hpp"

#include "Audio.hpp"
#include "Player.hpp"
#include "SpritePool.hpp"

PlayerReloadState::PlayerReloadState(Player &p_player) : PlayerWeaponState(p_player),
                                                         m_reloadDuration(SpritePool::PLAYER_RELOAD->GetDuration() + 0.2f) {}

void PlayerReloadState::OnEnterState() {
    m_player.m_animator->SetTrigger("reload");
    Audio::PlaySfx("reload");
    m_player.m_fireCooldown->Elapse();
    m_reloadDuration.Elapse();
}

void PlayerReloadState::Update() {
    m_reloadDuration.Update();
    if (!m_reloadDuration.IsElapsing()) {
        m_player.m_ammoCount = m_player.m_ammoMaxCount;
        m_player.m_ammoChangedCallback(m_player.m_ammoCount);
        m_player.SetCurrentWeaponState(m_player.m_weaponIdleState);
    }
}

