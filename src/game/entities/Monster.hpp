#pragma once

#include "Application.hpp"
#include "Entity.hpp"
#include "RenderWindow.hpp"
#include "Sprite.hpp"
#include "SpritePool.hpp"

#include <memory>

/**
 * @brief An enemy. Can follow a target and deal damage.
 */
class Monster : public Entity {

    public:
        explicit Monster(float p_x, float p_y, std::shared_ptr<Entity> p_target);

        void Update() override;
        
        void TakeDamage(unsigned int p_damageAmount) override;

        void SetTarget(std::shared_ptr<Entity> p_target);
        
    protected:
        void Die() override;
    
    private:
        std::shared_ptr<Entity> m_target;

        float m_hitDistance;
        float m_hitDamage;
        std::shared_ptr<Cooldown> m_hitCooldown;
};
