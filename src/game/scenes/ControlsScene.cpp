#include "ControlsScene.hpp"

#include "Alignment.hpp"
#include "Application.hpp"
#include "AssetExplorer.hpp"
#include "Audio.hpp"
#include "MenuScene.hpp"
#include "SpritePool.hpp"
#include "TextUi.hpp"

ControlsScene::ControlsScene() : Scene() {}

void ControlsScene::Load() {

    std::shared_ptr<UiEntity> controlsTitle = std::make_shared<UiEntity>(Vector2{0, 1},
                                                                         SpritePool::CONTROLS_TITLE->Clone(),
                                                                         Alignment::ALIGN_TOP |
                                                                         Alignment::ALIGN_CENTER);

    std::shared_ptr<TextUi> moveControls = std::make_shared<TextUi>(Vector2{5, 15}, "Move   Z   S");
    std::shared_ptr<TextUi> rotateControls = std::make_shared<TextUi>(Vector2{5, 25}, "Rotate   Q   D");
    std::shared_ptr<TextUi> shootControl = std::make_shared<TextUi>(Vector2{100, 15}, "Shoot");
    std::shared_ptr<TextUi> reloadControl = std::make_shared<TextUi>(Vector2{100, 25}, "Reload   R");
    std::shared_ptr<TextUi> back = std::make_shared<TextUi>(Vector2{-2, -2}, "Press       to go back",
                                                            Alignment::ALIGN_BOTTOM | Alignment::ALIGN_RIGHT);

    AddUi(controlsTitle);
    AddUi(moveControls);
    AddUi(rotateControls);
    AddUi(shootControl);
    AddUi(reloadControl);
    AddUi(back);
    AddUi(std::make_shared<UiEntity>(Vector2{36, 13}, SpritePool::KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{53, 13}, SpritePool::KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{45, 23}, SpritePool::KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{63, 23}, SpritePool::KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{134, 13}, SpritePool::ENTER_KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{138, 23}, SpritePool::KEY->Clone()));
    AddUi(std::make_shared<UiEntity>(Vector2{-55, -1}, SpritePool::BACK_KEY->Clone(),
                                     Alignment::ALIGN_BOTTOM | ALIGN_RIGHT));
}

void ControlsScene::Update() {
    if (Application::Get().IsKeyPressed(VK_BACK)) {
        Audio::PlaySfx("button_back");
        Application::Get().SetCurrentScene(std::make_shared<MenuScene>());
    }
}
