#pragma once

#include "Scene.hpp"

class CreditsScene : public Scene {

    public:
        explicit CreditsScene();

        void Load() override;

        void Update() override;

};


