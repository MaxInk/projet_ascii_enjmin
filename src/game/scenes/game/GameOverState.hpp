#pragma once

#include "Cooldown.hpp"
#include "GameState.hpp"

class GameOverState : public GameState {
        
    public:
        GameOverState(GameScene &p_gameScene);
        
        void OnEnterState() override;
        
        void Update() override;
        
    private:
        Cooldown m_overCooldown;
};


