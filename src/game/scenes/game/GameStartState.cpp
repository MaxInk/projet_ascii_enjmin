#include "GameStartState.hpp"

#include "Audio.hpp"
#include "GameScene.hpp"
#include "LevelFactory.hpp"
#include "Monster.hpp"
#include "MenuScene.hpp"

GameStartState::GameStartState(GameScene &p_gameScene) : GameState(p_gameScene), m_startCooldown(2.0f) {}

void GameStartState::OnEnterState() {
    // Game finished, no more level
    if (m_gameScene.m_levelIndex <= 0 || m_gameScene.m_levelIndex > 3) {
        Audio::StopBgm("doot");
        Application::Get().SetCurrentScene(std::make_shared<MenuScene>());
        return;
    }
    
    m_gameScene.m_level = LevelFactory::LoadLevel("level0" + std::to_string(m_gameScene.m_levelIndex));
    if (!m_gameScene.m_level) {
        Application::Get().SetCurrentScene(std::make_shared<MenuScene>());
        return;
    }

    // Reset game logic
    m_gameScene.m_monsterCount = 0;
    m_gameScene.m_player = std::make_shared<Player>(0, 0, 0);
    m_gameScene.m_maxDist = std::hypotf(m_gameScene.m_level->GetWidth(), m_gameScene.m_level->GetHeight());

    // Reset monsters and set starting position
    m_gameScene.m_entities.clear();
    for (size_t y(0); y < m_gameScene.m_level->GetHeight(); ++y) {
        for (size_t x(0); x < m_gameScene.m_level->GetWidth(); ++x) {
            Level::Square square = m_gameScene.m_level->GetSquare(x, y);
            if (square == Level::Square::MONSTER) {
                std::shared_ptr<Entity> monster = std::make_shared<Monster>(x, y, m_gameScene.m_player);
                monster->OnHealthChanged([this](unsigned int p_newHealth){
                    if (p_newHealth == 0) {
                        m_gameScene.m_monsterCount--;
                    }
                });
                m_gameScene.AddEntity(monster);
                m_gameScene.m_monsterCount++;
            } else if (square == Level::Square::START) {
                m_gameScene.m_player->SetPosition(x, y);
            }
        }
    }

    // Reset UI
    m_gameScene.m_lifeBar->SetMaxHealth(100);
    m_gameScene.m_lifeBar->SetCurrentHealth(100);
    m_gameScene.m_ammoText->SetText("5/5");
    m_gameScene.m_levelStateText->SetSprite(GetLevelText(m_gameScene.m_levelIndex));

    m_gameScene.m_player->OnHealthChanged([this](unsigned int p_newHealth) {
        m_gameScene.m_lifeBar->SetCurrentHealth(p_newHealth);
        Audio::PlaySfx("player_receives_damage");
        if (p_newHealth == 0) {
            m_gameScene.SetCurrentGameState(m_gameScene.m_overState);
        }
    });

    m_gameScene.m_player->OnAmmoChanged([this](unsigned int p_newAmmoCount) {
        m_gameScene.m_ammoText->SetText(std::to_string(p_newAmmoCount) + "/" + std::to_string(m_gameScene.m_player->GetAmmoMaxCount()));
    });

    // Reset SFX
    m_gameScene.m_player->OnStep([](){
        Audio::PlaySfx("foot_step");
    });
    m_gameScene.m_player->OnShooting([]() {
        Audio::PlaySfx("gun_shot");
    });
    
    m_startCooldown.Elapse();
}

void GameStartState::Update() {
    if (m_startCooldown.IsElapsing()) {
        m_startCooldown.Update();
        return;
    }
    m_gameScene.SetCurrentGameState(m_gameScene.m_runState);
}

std::shared_ptr<const Sprite> GameStartState::GetLevelText(unsigned int p_levelIndex) {
    switch (p_levelIndex) {
        case 1: return SpritePool::LEVEL_01;
        case 2: return SpritePool::LEVEL_02;
        case 3: return SpritePool::LEVEL_03;
        default: return SpritePool::LEVEL_01;
    }
}
