#pragma once

#include "Cooldown.hpp"
#include "GameState.hpp"
#include "Sprite.hpp"

class GameStartState : public GameState {

    public:
        explicit GameStartState(GameScene &p_gameScene);
        
        void OnEnterState() override;
        
        void Update() override;

    private:
        static std::shared_ptr<const Sprite> GetLevelText(unsigned int p_levelIndex);

    private:
        Cooldown m_startCooldown;
};
