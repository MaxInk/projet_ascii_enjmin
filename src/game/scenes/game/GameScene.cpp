#include "GameScene.hpp"

#include "Alignment.hpp"
#include "Audio.hpp"
#include "GameFinishState.hpp"
#include "GameOverState.hpp"
#include "GameRunState.hpp"
#include "GameStartState.hpp"
#include "LevelFactory.hpp"
#include "Monster.hpp"

const char GameScene::GRADIENT_CHAR[17] = {' ', '.', ':', ';', '_', '*', '-', '+', '=', 'u', 'x', '/', '5', 'X', '8', '$', '#'};
const float GameScene::FOV = 3.14159f / 3.0f;
const float GameScene::DEPTH = 25.0f;

GameScene::GameScene()
        : m_entities(), m_monsterCount(0), m_levelIndex(1), m_level(nullptr), m_player(nullptr),
          m_currentGameState(nullptr), m_startState(std::make_shared<GameStartState>(*this)),
          m_runState(std::make_shared<GameRunState>(*this)), m_finishState(std::make_shared<GameFinishState>(*this)),
          m_overState(std::make_shared<GameOverState>(*this)),
          m_levelStateText(std::make_shared<UiEntity>(Vector2{0, 0}, nullptr, Alignment::ALIGN_CENTER)),
          m_ammoText(std::make_shared<TextUi>(Vector2{-50, -1}, "", Alignment::ALIGN_BOTTOM | ALIGN_RIGHT)),
          m_lifeBar(std::make_shared<LifeBarUi>(Vector2{5, -1}, Alignment::ALIGN_BOTTOM)),
          m_maxDist(), m_lotDistance(Application::SCREEN_WIDTH) {
}

void GameScene::Load() {
    // Load static UI
    std::shared_ptr<UiEntity> crossHair = std::make_shared<UiEntity>(Vector2{0, 0},
                                                                     SpritePool::CROSSHAIR->Clone(),
                                                                     Alignment::ALIGN_CENTER);
    AddUi(m_lifeBar);
    AddUi(m_ammoText);
    AddUi(crossHair);
    AddUi(m_levelStateText);
    
    SetCurrentGameState(m_startState);
}

void GameScene::AddEntity(std::shared_ptr<Entity> &p_entity) {
    m_entities.push_back(p_entity);
}

void GameScene::Update() {
    m_currentGameState->Update();
}

void GameScene::Render(RenderWindow &p_renderWindow) {
    RenderWorld(p_renderWindow);
    for (std::shared_ptr<Entity> &entity: m_entities) {
        RenderEntity(p_renderWindow, entity);
    }
    RenderPlayer(p_renderWindow);
    //RenderMinimap(p_renderWindow); // Only for developers :)
    Scene::Render(p_renderWindow);

}

bool GameScene::IsItColliding(float p_positionX, float p_positionY) const {
    return m_level->GetSquare(p_positionX, p_positionY) == Level::Square::WALL;
}

std::shared_ptr<Entity> GameScene::GetPointingEntity() const {
    for (const std::shared_ptr<Entity> &entity : m_entities) {
        if (entity->GetHealth() > 0 && CheckGunDirection(entity)) {
            return entity;
        }
    }
    return nullptr;
}

void GameScene::RenderEntity(RenderWindow &p_renderWindow, std::shared_ptr<Entity> &p_entity) {
    if (!p_entity->GetSprite()) {
        return;
    }
    const Vector2 &entityPosition(p_entity->GetPosition());
    Vector2 playerDirection(Vector2{sinf(m_player->GetOrientation()), cosf(m_player->GetOrientation())});
    Vector2 directionToMe(Vector2{entityPosition.x - m_player->GetPosition().x, entityPosition.y - m_player->GetPosition().y});

    float distanceToPlayer(directionToMe.GetMagnitude());
    directionToMe.Normalize();

    Vector2 normals(Vector2{-directionToMe.y, directionToMe.x});

    float cosAngleNormals(fmin(playerDirection.GetAngle(normals), 1.0f));
    float cosAngle(fmin(playerDirection.GetAngle(directionToMe), 1.0f));
    float angle(acosf(cosAngle));

    float x(p_renderWindow.GetRenderWidth() / 2 + sinf(angle) * (2.0f/distanceToPlayer) * (cosAngleNormals < 0 ? 1 : -1) * (p_renderWindow.GetRenderWidth() / 2));
    if (-angle < FOV / 2 && -angle > -FOV / 2) {
        p_renderWindow.DrawEntity(p_entity->GetSprite(),x,
                                  p_renderWindow.GetRenderHeight() / 2,
                                  m_lotDistance, distanceToPlayer, m_maxDist);
    }
}

void GameScene::RenderWorld(RenderWindow &p_renderWindow) {
    size_t width(p_renderWindow.GetRenderWidth());
    for (size_t x(0); x <= width; x++) {
        double wallHeight(ComputeWallHeight(m_player->GetPosition().x, m_player->GetPosition().y,
                                            m_player->GetOrientation(), x, width));
        p_renderWindow.DrawVerticalLine(GetGradientColor(wallHeight, 120), width - x, wallHeight);
    }
}

void GameScene::RenderMinimap(RenderWindow &p_renderWindow) {
    for (size_t y(0); y < m_level->GetHeight(); y++) {
        for (size_t x(0); x < m_level->GetWidth(); x++) {
            Level::Square square(m_level->GetSquare(x, y));
            p_renderWindow.PutChar((char) square, x, y + 1);
        }
    }
    // Render player m_position, orientation and monsters
    int mapPosX(round(m_player->GetPosition().x));
    int mapPosY(round(m_player->GetPosition().y));
    p_renderWindow.PutChar('O', FOREGROUND_RED, mapPosX, mapPosY);

    p_renderWindow.PutChar('X', FOREGROUND_GREEN, mapPosX + round(sinf(m_player->GetOrientation())),
                           mapPosY + round(cosf(m_player->GetOrientation())));

    for (const std::shared_ptr<Entity> &entity: m_entities) {
        p_renderWindow.PutChar('W', FOREGROUND_BLUE, entity->GetPosition().x, entity->GetPosition().y);
    }
}

void GameScene::RenderPlayer(RenderWindow &p_renderWindow) {
    std::shared_ptr<const Sprite> sprite(m_player->GetSprite());
    p_renderWindow.DrawSprite(sprite, p_renderWindow.GetRenderWidth() - sprite->GetWidth(),
                              p_renderWindow.GetRenderHeight() - sprite->GetHeight());
}

bool GameScene::CheckGunDirection(std::shared_ptr<Entity> p_entity) const {
    if (p_entity == nullptr) {
        return false;
    }
    const Vector2 &entityPosition(p_entity->GetPosition());
    Vector2 playerDirection(Vector2{sinf(m_player->GetOrientation()), cosf(m_player->GetOrientation())});

    Vector2 directionToMe(Vector2{entityPosition.x - m_player->GetPosition().x, entityPosition.y - m_player->GetPosition().y});
    float distanceToPlayer(directionToMe.GetMagnitude());
    directionToMe.Normalize();

    float cosAngle(fmin(playerDirection.GetAngle(directionToMe), 1.0f));
    float angle(acosf(cosAngle));
    
    return distanceToPlayer < m_lotDistance[Application::SCREEN_WIDTH/2] && angle >= -0.1f && angle <= 0.1f;
}

double GameScene::ComputeWallHeight(float p_playerX, float p_playerY, float p_playerOrientation, size_t p_x, int p_width) {
    float rayAngle((p_playerOrientation - FOV / 2.0f) + (p_x / (float) p_width) * FOV);
    float distance(0.0f);
    bool hitWall(false);

    Vector2 playerDirection{sinf(rayAngle), cosf(rayAngle)};
    Vector2 test{0.0f, 0.0f};
    while (!hitWall && distance < DEPTH) {
        test.x = p_playerX + playerDirection.x * distance;
        test.y = p_playerY + playerDirection.y * distance;
        // std::cout << "Check p_x: " << testX << " y: " << testY << std::endl;
        if (test.x < 0 || test.x > m_level->GetWidth() || test.y < 0 || test.y > m_level->GetHeight()) {
            hitWall = true;
            distance = -1;
        } else if (m_level->GetSquare(test.x, test.y) == Level::Square::WALL) {
            hitWall = true;
        }
        distance += 0.1f;
    }
    m_lotDistance[p_x] = distance < 0 ? 9999999 : distance;

    int wallHeight = (distance == -1) ? 0 : 80.0f / distance;
    wallHeight *= cos(FOV / 2.0f);
    return wallHeight;
}

char GameScene::GetGradientColor(int p_index, int p_maxValues) {
    size_t color(std::min(std::max(p_index * 13 / p_maxValues, 0), 16));
    return GRADIENT_CHAR[color];
}

void GameScene::SetCurrentGameState(std::shared_ptr<GameState> p_gameState) {
    m_currentGameState = p_gameState;
    m_currentGameState->OnEnterState();
}
