#pragma once

class GameScene;

class GameState {

    public:
        explicit GameState(GameScene &p_gameScene);

        virtual void OnEnterState() = 0;

        virtual void Update() = 0;

    protected:
        GameScene &m_gameScene;
};
