#pragma once

#include "GameState.hpp"

class GameRunState : public GameState {

    public:
        explicit GameRunState(GameScene &p_gameScene);
        
        void OnEnterState() override;
        
        void Update() override;
};


