#pragma once
#include "Entity.hpp"
#include "GameState.hpp"
#include "Level.hpp"
#include "LifeBarUi.hpp"
#include "Player.hpp"
#include "Scene.hpp"
#include "TextUi.hpp"
#include "UiEntity.hpp"

#include <memory>
#include <vector>

class GameStartState;
class GameRunState;
class GameFinishState;
class GameOverState;

class GameScene : public Scene {
        
    public:
        explicit GameScene();
        
        void Load() override;
        
        void Update() override;
        
        void Render(RenderWindow &p_renderWindow) override;

        bool IsItColliding(float p_positionX, float p_positionY) const;
        
        std::shared_ptr<Entity> GetPointingEntity() const;

    protected:
        void AddEntity(std::shared_ptr<Entity> &p_entity);

    private:
        void RenderEntity(RenderWindow &p_renderWindow, std::shared_ptr<Entity> &p_entity);

        void RenderWorld(RenderWindow &p_renderWindow);

        void RenderMinimap(RenderWindow &p_renderWindow);

        void RenderPlayer(RenderWindow &p_renderWindow);

        bool CheckGunDirection(std::shared_ptr<Entity> p_entity) const;

        double ComputeWallHeight(float p_playerX, float p_playerY, float p_playerOrientation, size_t p_x, int p_width);

        static char GetGradientColor(int p_index, int p_maxValues);
        
        void SetCurrentGameState(std::shared_ptr<GameState> p_gameState);

    private:
        static const char GRADIENT_CHAR[17];
        static const float FOV;
        static const float DEPTH;
        
        // Level
        std::vector<std::shared_ptr<Entity>> m_entities;
        unsigned int m_monsterCount;
        unsigned int m_levelIndex;
        std::unique_ptr<Level> m_level;
        std::shared_ptr<Player> m_player;
        
        // Game states
        std::shared_ptr<GameState> m_currentGameState;
        std::shared_ptr<GameState> m_startState;
        std::shared_ptr<GameState> m_runState;
        std::shared_ptr<GameState> m_finishState;
        std::shared_ptr<GameState> m_overState;

        // UI
        std::shared_ptr<UiEntity> m_levelStateText;
        std::shared_ptr<TextUi> m_ammoText;
        std::shared_ptr<LifeBarUi> m_lifeBar;
        
        // Render
        int m_maxDist;
        std::vector<double> m_lotDistance;
        
        friend GameStartState;
        friend GameRunState;
        friend GameFinishState;
        friend GameOverState;
};
