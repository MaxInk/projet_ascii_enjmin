#include "GameFinishState.hpp"

#include "Audio.hpp"
#include "GameScene.hpp"
#include "SpritePool.hpp"

GameFinishState::GameFinishState(GameScene &p_gameScene) : GameState(p_gameScene), m_levelClearedCooldown(3.0f) {}

void GameFinishState::OnEnterState() {
    m_gameScene.m_levelStateText->SetSprite(SpritePool::LEVEL_CLEARED);
    m_gameScene.m_levelStateText->SetVisible(true);
    Audio::PlaySfx("win");
    m_levelClearedCooldown.Elapse();
}

void GameFinishState::Update() {
    // Continue the animations
    m_gameScene.m_player->UpdateComponents();
    for (std::shared_ptr<Entity> &entity: m_gameScene.m_entities) {
        entity->UpdateComponents();
    }
    
    if (m_levelClearedCooldown.IsElapsing()) {
        m_levelClearedCooldown.Update();
        return;
    }
    m_gameScene.m_levelIndex++;
    m_gameScene.SetCurrentGameState(m_gameScene.m_startState);
}
