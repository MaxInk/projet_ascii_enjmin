#include "GameOverState.hpp"

#include "Application.hpp"
#include "Audio.hpp"
#include "SpritePool.hpp"

GameOverState::GameOverState(GameScene &p_gameScene) : GameState(p_gameScene), m_overCooldown(3.0f) {}

void GameOverState::OnEnterState() {
    m_gameScene.m_levelStateText->SetSprite(SpritePool::YOU_ARE_DEAD);
    m_gameScene.m_levelStateText->SetVisible(true);
    Audio::StopBgm("doot");
    Audio::PlaySfx("lose");
    m_overCooldown.Elapse();
}

void GameOverState::Update() {
    // Continue the animations
    for (std::shared_ptr<Entity> &entity: m_gameScene.m_entities) {
        entity->UpdateComponents();
    }

    if (m_overCooldown.IsElapsing()) {
        m_overCooldown.Update();
        return;
    }
    m_gameScene.m_levelIndex = 0; // No more level
    m_gameScene.SetCurrentGameState(m_gameScene.m_startState);
}
