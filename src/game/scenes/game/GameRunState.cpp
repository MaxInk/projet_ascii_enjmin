#include "GameRunState.hpp"

#include "AssetExplorer.hpp"
#include "Audio.hpp"
#include "GameScene.hpp"

GameRunState::GameRunState(GameScene &p_gameScene) : GameState(p_gameScene) {}

void GameRunState::OnEnterState() {
    if (m_gameScene.m_levelIndex == 1) {
        Audio::PlayBgm("doot"); // Start bgm only for the first level (bgm is looping)sz
    }
    m_gameScene.m_levelStateText->SetVisible(false);
}

void GameRunState::Update() {
    m_gameScene.m_player->UpdateComponents();
    m_gameScene.m_player->Update();

    for (std::shared_ptr<Entity> &entity: m_gameScene.m_entities) {
        entity->UpdateComponents();
        entity->Update();
    }
    if (m_gameScene.m_monsterCount == 0) {
        m_gameScene.SetCurrentGameState(m_gameScene.m_finishState);
    }
}
