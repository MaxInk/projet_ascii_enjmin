#pragma once

#include "Cooldown.hpp"
#include "GameState.hpp"

class GameFinishState : public GameState {

    public:
        explicit GameFinishState(GameScene &p_gameScene);
        
        void OnEnterState() override;
        
        void Update() override;

    private:
        Cooldown m_levelClearedCooldown;
};
