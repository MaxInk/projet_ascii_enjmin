#pragma once

#include "Scene.hpp"

class ControlsScene : public Scene {

    public:
        explicit ControlsScene();

        void Load() override;

        void Update() override;
};


