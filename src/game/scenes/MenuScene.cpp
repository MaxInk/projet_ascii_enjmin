#include "MenuScene.hpp"

#include "Alignment.hpp"
#include "Application.hpp"
#include "AssetExplorer.hpp"
#include "Audio.hpp"
#include "ControlsScene.hpp"
#include "CreditsScene.hpp"
#include "SpritePool.hpp"

MenuScene::MenuScene()
        : Scene(), m_currentButtonFocusedIndex(), m_buttons(), m_changeButtonCooldown(0.3f) {}

void MenuScene::Load() {
    std::shared_ptr<UiEntity> title = std::make_shared<UiEntity>(Vector2{0, 1}, SpritePool::TODOOM_TITLE->Clone(),
                                                                   Alignment::ALIGN_TOP | Alignment::ALIGN_CENTER);

    std::shared_ptr<ButtonUi> playButton = std::make_shared<ButtonUi>(Vector2{0, -5}, "PLAY", Alignment::ALIGN_CENTER);
    std::shared_ptr<ButtonUi> controlsButton = std::make_shared<ButtonUi>(Vector2{0, 3}, "CONTROLS", Alignment::ALIGN_CENTER);
    std::shared_ptr<ButtonUi> creditsButton = std::make_shared<ButtonUi>(Vector2{0, 11}, "CREDITS", Alignment::ALIGN_CENTER);
    std::shared_ptr<ButtonUi> leaveButton = std::make_shared<ButtonUi>(Vector2{0, 19}, "QUIT", Alignment::ALIGN_CENTER);

    playButton->OnButtonPressed([](){
        Application::Get().SetCurrentScene(std::make_shared<GameScene>());
    });
    
    controlsButton->OnButtonPressed([](){
        Application::Get().SetCurrentScene(std::make_shared<ControlsScene>());
    });

    creditsButton->OnButtonPressed([](){
        Application::Get().SetCurrentScene(std::make_shared<CreditsScene>());
    });
    
    leaveButton->OnButtonPressed([](){
        Application::Get().Stop();
    });
    
    m_buttons.reserve(4);
    m_buttons.push_back(playButton);
    m_buttons.push_back(controlsButton);
    m_buttons.push_back(creditsButton);
    m_buttons.push_back(leaveButton);

    FocusButton(0, false);
    
    AddUi(title);
    AddUi(playButton);
    AddUi(controlsButton);
    AddUi(creditsButton);
    AddUi(leaveButton);
}

void MenuScene::Update() {
    m_changeButtonCooldown.Update();
    
    if (Application::Get().IsKeyPressed(VK_RETURN)) {
        Audio::PlaySfx("button_pressed");
        m_buttons[m_currentButtonFocusedIndex]->Press();
        return;
    }
    
    if (!IsDownKeyPressed() && !IsUpKeyPressed()) {
        m_changeButtonCooldown.Stop();
    }
    if (m_changeButtonCooldown.IsElapsing()) {
        return;
    }
    
    if (IsDownKeyPressed()) {
        FocusNextButton();
    } else if (IsUpKeyPressed()) {
        FocusLastButton();
    }
}

void MenuScene::FocusButton(size_t p_buttonIndex, bool p_playSound) {
    m_buttons[m_currentButtonFocusedIndex]->SetFocused(false);
    m_currentButtonFocusedIndex = p_buttonIndex;
    m_buttons[m_currentButtonFocusedIndex]->SetFocused(true);
    m_changeButtonCooldown.Elapse();
    if (p_playSound) {
        Audio::PlaySfx("button_selected");
    }
}

void MenuScene::FocusNextButton() {
    if (m_currentButtonFocusedIndex < m_buttons.size() - 1) {
        FocusButton(m_currentButtonFocusedIndex + 1);
    } else {
        FocusButton(0);
    }
}

void MenuScene::FocusLastButton() {
    if (m_currentButtonFocusedIndex > 0) {
        FocusButton(m_currentButtonFocusedIndex - 1);
    } else {
        FocusButton(m_buttons.size() - 1);
    }
}

bool MenuScene::IsUpKeyPressed() {
    return Application::Get().IsKeyPressed('Z') || Application::Get().IsKeyPressed(VK_UP);
}

bool MenuScene::IsDownKeyPressed() {
    return Application::Get().IsKeyPressed('S') || Application::Get().IsKeyPressed(VK_DOWN);
}
