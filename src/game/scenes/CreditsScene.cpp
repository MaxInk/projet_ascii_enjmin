#include "CreditsScene.hpp"

#include "Alignment.hpp"
#include "Application.hpp"
#include "AssetExplorer.hpp"
#include "Audio.hpp"
#include "MenuScene.hpp"
#include "SpritePool.hpp"

CreditsScene::CreditsScene() : Scene() {}

void CreditsScene::Load() {
    std::shared_ptr<UiEntity> creditsTitle = std::make_shared<UiEntity>(Vector2{0, 1},
                                                                         SpritePool::CREDITS_TITLE->Clone(),
                                                                         Alignment::ALIGN_TOP |
                                                                         Alignment::ALIGN_CENTER);

    std::shared_ptr<TextUi> back = std::make_shared<TextUi>(Vector2{-2, -2}, "Press       to go back",
                                                            Alignment::ALIGN_BOTTOM | Alignment::ALIGN_RIGHT);
    
    AddUi(creditsTitle);
    AddUi(std::make_shared<TextUi>(Vector2{10, 20}, "Simon FEUGEAS"));
    AddUi(std::make_shared<TextUi>(Vector2{10, 30}, "Maxime MAURIN"));
    AddUi(back);
    AddUi(std::make_shared<UiEntity>(Vector2{-55, -1}, SpritePool::BACK_KEY->Clone(),
                                     Alignment::ALIGN_BOTTOM | ALIGN_RIGHT));
}

void CreditsScene::Update() {
    if (Application::Get().IsKeyPressed(VK_BACK)) {
        Audio::PlaySfx("button_back");
        Application::Get().SetCurrentScene(std::make_shared<MenuScene>());
    }
}
