#pragma once

#include "ButtonUi.hpp"
#include "Cooldown.hpp"
#include "Scene.hpp"

class MenuScene : public Scene {

    public:
        explicit MenuScene();
        
        void Load() override;

        void Update() override;

    private:
        void FocusButton(size_t p_buttonIndex, bool p_playSound = true);
        
        void FocusNextButton();
        
        void FocusLastButton();
        
        static bool IsUpKeyPressed();

        static bool IsDownKeyPressed();

    private:
        size_t m_currentButtonFocusedIndex;
        std::vector<std::shared_ptr<ButtonUi>> m_buttons;
        Cooldown m_changeButtonCooldown;
};
