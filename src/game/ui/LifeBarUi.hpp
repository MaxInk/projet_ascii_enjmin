#pragma once

#include "UiEntity.hpp"

/**
 * @brief Life bar UI element that is Updated with the Player health
 * @see Player
 * @see UiEntity
 * 
 */
class LifeBarUi : public UiEntity {

    public:
        explicit LifeBarUi(const Vector2 &p_position, unsigned int p_alignmentFlag = 0);

        int GetMaxHealth() const;

        void SetMaxHealth(unsigned int p_maxHealth);

        int GetCurrentHealth() const;

        void SetCurrentHealth(unsigned int p_currentHealth);

    private:
        void UpdateSprite();

    private:
        unsigned int m_maxHealth;
        unsigned int m_currentHealth;
};
