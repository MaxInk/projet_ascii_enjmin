#include "LifeBarUi.hpp"

#include "SpritePool.hpp"
#include "UnicodeChar.hpp"

#define HEALTH_WIDTH 32
#define HEALTH_HEIGHT 4
#define HEALTH_X_START 14
#define HEALTH_Y_START 2

LifeBarUi::LifeBarUi(const Vector2 &p_position, unsigned int p_alignmentFlag)
        : UiEntity(p_position, SpritePool::LIFE_BAR->Clone(), p_alignmentFlag), m_maxHealth(100), m_currentHealth(100) {
    m_sprite->GetChar(12, 5).Char.UnicodeChar = EMPTY_CHAR;
    m_sprite->GetChar(13, 5).Char.UnicodeChar = EMPTY_CHAR;
}

int LifeBarUi::GetMaxHealth() const {
    return m_maxHealth;
}

void LifeBarUi::SetMaxHealth(unsigned int p_maxHealth) {
    m_maxHealth = fmax(0, p_maxHealth);
    if (m_currentHealth > m_maxHealth) {
        m_currentHealth = m_maxHealth;
    }
    UpdateSprite();
}

int LifeBarUi::GetCurrentHealth() const {
    return m_currentHealth;
}

void LifeBarUi::SetCurrentHealth(unsigned int p_currentHealth) {
    m_currentHealth = fmin(p_currentHealth, m_maxHealth);
    UpdateSprite();
}

void LifeBarUi::UpdateSprite() {
    int relativeWidth(HEALTH_WIDTH * m_currentHealth / m_maxHealth);
    for (int y = HEALTH_Y_START; y < HEALTH_Y_START + HEALTH_HEIGHT; ++y) {
        for (int x = HEALTH_X_START; x < HEALTH_X_START + relativeWidth; ++x) {
            m_sprite->GetChar(x, y).Char.AsciiChar = '$';
        }
        for (int x = HEALTH_X_START + relativeWidth; x < HEALTH_X_START + HEALTH_WIDTH; ++x) {
            m_sprite->GetChar(x, y).Char.UnicodeChar = EMPTY_CHAR;
        }
    }
}
