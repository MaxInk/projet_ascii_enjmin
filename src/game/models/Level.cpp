#include "Level.hpp"

Level::Level(size_t p_width, size_t p_height, const std::vector<Level::Square> &p_map) : m_width(p_width), m_height(p_height),
                                                                      m_map(p_map) {}


Level::Square Level::GetSquare(const int p_x, const int p_y) {
    return m_map[p_x + p_y * m_width];
}

size_t Level::GetWidth() const {
    return m_width;
}

size_t Level::GetHeight() const {
    return m_height;
}



