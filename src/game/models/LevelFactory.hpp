#pragma once

#include "AssetExplorer.hpp"
#include "Level.hpp"

#include <fstream>
#include <iostream>
#include <memory>

/**
 * @brief Generates Level Object from .level files
 * @see Level
 */
class LevelFactory {

    public:
        explicit LevelFactory() = delete;

        explicit LevelFactory(const LevelFactory &p_levelFactory) = delete;

        LevelFactory &operator=(const LevelFactory &p_levelFactory) = delete;

        ~LevelFactory() = delete;

        static std::unique_ptr<Level> LoadLevel(const std::string &p_levelName) {
            std::string filePath(AssetExplorer::GetLevelPath(p_levelName));
            std::ifstream file;
            file.open(filePath, std::fstream::in);
            if (!file.is_open()) {
                std::cerr << "File note found : " << filePath << "." << std::endl;
                return nullptr;
            }

            size_t lineCount;
            size_t columnCount;
            file >> lineCount;
            file >> columnCount;

            std::vector<Level::Square> map(lineCount * columnCount);
            size_t flag;

            for (size_t y(0); y < lineCount; ++y) {
                for (size_t x(0); x < columnCount; ++x) {
                    if (file >> flag) {
                        map[x + y * columnCount] = ToLevelSquare(flag);
                    } else {
                        std::cerr << "Error in level parsing." << std::endl;
                        return nullptr;
                    }
                }
            }
            file.close();
            return std::make_unique<Level>(columnCount, lineCount, map);
        }

    private:
        static Level::Square ToLevelSquare(const size_t p_flag) {
            switch (p_flag) {
                case 0:
                    return Level::Square::NONE;
                case 1:
                    return Level::Square::WALL;
                case 2:
                    return Level::Square::MONSTER;
                case 3:
                    return Level::Square::START;
                default:
                    return Level::Square::UNKNOWN;
            }
        }
};
