#include <src/engine/core/Application.hpp>

int main() {
    Application::Get().Run();
    return EXIT_SUCCESS;
}
