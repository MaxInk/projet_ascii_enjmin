#pragma once

#include <cmath>

/**
 * @brief A 2-dimensional mathematical vector. Addition, subtraction, dot product, normalization
 * and distance operations can be performed on this class.
 */
struct Vector2 {
    float x;
    float y;

    Vector2 operator+(const Vector2 &p_vector) const {
        return {x + p_vector.x, y + p_vector.y};
    }

    Vector2 operator-(const Vector2 &p_vector) const {
        return {x - p_vector.x, y - p_vector.y};
    }

    Vector2 &operator+=(const Vector2 &p_vector) {
        x += p_vector.x;
        y += p_vector.y;
        return *this;
    }

    float GetMagnitude() const {
        return sqrt(x * x + y * y);
    }

    float GetDotProduct(const Vector2 &p_vector) const {
        return (x * p_vector.x + y * p_vector.y);
    }

    float GetAngle(const Vector2 &p_vector) const {
        return GetDotProduct(p_vector) / GetMagnitude() * p_vector.GetMagnitude();
    }

    void Normalize() {
        float mag = GetMagnitude();
        x /= mag;
        y /= mag;
    }

    float Distance(const Vector2 &p_vector) const {
        return sqrt((x - p_vector.x) * (x - p_vector.x) + (y - p_vector.y) * (y - p_vector.y));
    }
};
