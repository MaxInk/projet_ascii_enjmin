#pragma once

#include <string>

/**
 * @brief Retrieves project assets by name
 */
class AssetExplorer {

    public:
        explicit AssetExplorer() = delete;

        explicit AssetExplorer(const AssetExplorer &p_assetExplorer) = delete;

        AssetExplorer &operator=(const AssetExplorer &p_assetExplorer) = delete;
        
        ~AssetExplorer() = delete;

        static std::string GetLevelPath(const std::string &p_levelName) {
            return "./assets/levels/" + p_levelName + ".level";
        }

        static std::string GetSpritePath(const std::string &p_spriteName) {
            return "./assets/sprites/" + p_spriteName + ".sprite";
        }

        static std::string GetAnimationPath(const std::string &p_animationName) {
            return "./assets/animations/" + p_animationName + ".animation";
        }

        static std::string GetFontPath(const std::string &p_fontName) {
            return "./assets/fonts/" + p_fontName + ".font";
        }

        static std::string GetSfxPath(const std::string &p_sfxName) {
            return "./assets/audios/SFX/" + p_sfxName + ".wav";
        }

        static std::string GetBgmPath(const std::string &p_bgmPath) {
            return "./assets/audios/BGM/" + p_bgmPath + ".wav";
        }
};
